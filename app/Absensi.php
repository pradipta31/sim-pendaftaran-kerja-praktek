<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    protected $fillable = [
        'admin_id',
        'mahasiswa_id',
        'jurnal_id',
        'tanggal',
        'status'
    ];

    public function admin(){
        return $this->belongsTo('App\Admin');
    }

    public function mahasiswa(){
        return $this->belongsTo('App\Mahasiswa');
    }

    public function jurnal(){
        return $this->belongsTo('App\Jurnal');
    }

    public $timestamps = true;
}
