<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $fillable = [
        'user_id',
        'nip',
        'tanggal_lahir',
        'no_hp',
        'alamat'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public $timestamps = true;
}
