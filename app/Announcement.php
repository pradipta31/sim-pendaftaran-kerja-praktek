<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $fillable = [
        'admin_id', 'judul', 'isi', 'tgl_mulai', 'tgl_akhir', 'status'
    ];

    public function admin(){
        return $this->belongsTo('App\Admin');
    }

    public $timestamps = true;
}
