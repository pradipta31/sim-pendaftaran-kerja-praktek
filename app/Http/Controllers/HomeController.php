<?php

namespace App\Http\Controllers;

use Auth;
use App\Jurnal;
use App\Mahasiswa;
use App\Pendaftaran;
use App\Announcement;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->level == 1) {
            toastSuccess('Login berhasil!');
            $pengumumans = Announcement::where('status',1)->get();
            $countPane = [
                'validasi_jurnal' => Jurnal::where('status', 2)->count(),
                'mahasiswa' => Mahasiswa::where('status', 1)->count(),
                'pendaftaran' => Pendaftaran::all()->count(),
                'bidang_1' => Pendaftaran::where('bidang', 1)->count(),
                'bidang_2' => Pendaftaran::where('bidang', 2)->count(),
                'bidang_3' => Pendaftaran::where('bidang', 3)->count()
            ];
            return view('petugas.dashboard', compact('pengumumans'), $countPane);
        }elseif(Auth::user()->level == 2){
            toastSuccess('Login berhasil!');
            $countPane = [
                'validasi_jurnal' => Jurnal::where('status', 2)->count(),
                'mahasiswa' => Mahasiswa::where('status', 1)->count(),
                'pendaftaran' => Pendaftaran::all()->count(),
                'bidang_1' => Pendaftaran::where('bidang', 1)->count(),
                'bidang_2' => Pendaftaran::where('bidang', 2)->count(),
                'bidang_3' => Pendaftaran::where('bidang', 3)->count()
            ];
            $pengumumans = Announcement::where('status',1)->get();
            return view('kabag.dashboard', compact('pengumumans'), $countPane);
        }elseif (Auth::user()->level == 3) {
            toastSuccess('Login berhasil!');
            $pengumumans = Announcement::where('status',1)->get();
            return view('mahasiswa.dashboard', compact('pengumumans'));
        }
    }
}
