<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JsonController extends Controller
{
    public function index(){
        $path = base_path() . "/storage/app/public/json/messages.json"; // ie: /var/www/laravel/app/storage/json/filename.json

        $json = json_decode(file_get_contents($path), true);
        //var_dump($json);

        foreach ($json as $key => $value) {
            //  dd($value);
            //dd($value['conversation']);
            $con = $value['conversation'];
            foreach ($con as $item) {
                echo "<table style='border: 1px solid black'>
                        <tr>
                            <th style='border: 1px solid black; width: 300px'>Pengirim</th>
                            <th style='border: 1px solid black; '>tanggal</th>
                            <th style='border: 1px solid black; width: 50%'>Pesan</th>
                        </tr>
                        <tr>
                            <td style='border: 1px solid black'>".$item['sender']."</td>
                            <td style='border: 1px solid black'>".date('d-m-Y - h:i:s', strtotime($item['created_at']))."</td>
                            <td style='border: 1px solid black'>".$item['text']."</td>
                        </tr>
                    </table>";
            }
            // dd($con);
            // return view('JSON.index', compact('con'));
        }
    }
}
