<?php

namespace App\Http\Controllers\Kabag;

use App\Mahasiswa;
use App\Jurnal;
use App\Absensi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class JurnalController extends Controller
{
    public function showMhs(){
        $no = 1;
        $mahasiswas = Mahasiswa::select('mahasiswas.*','pendaftarans.*','users.*')
                      ->join('pendaftarans', 'pendaftarans.id','=','mahasiswas.pendaftaran_id')
                      ->join('users', 'users.id', '=', 'mahasiswas.user_id')
                      ->where('pendaftarans.status', '=',1)
                      ->get();
        return view('kabag.jurnal.mahasiswa', compact('no','mahasiswas'));
    }

    public function indexJurnal($id){
        $no = 1;
        $mhs = Mahasiswa::where('user_id', $id)->first();
        $jurnals = Jurnal::where('mahasiswa_id', $mhs->id)->get();
        return view('kabag.jurnal.jurnal', compact('no', 'jurnals'));
    }

    public function indexAbsen($id){
        $no = 1;
        $mhs = Mahasiswa::where('user_id', $id)->first();
        $absents = Absensi::where('mahasiswa_id', $mhs->id)->get();
        return view('kabag.jurnal.absen', compact('no', 'absents'));
    }
}
