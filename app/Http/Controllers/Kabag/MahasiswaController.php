<?php

namespace App\Http\Controllers\Kabag;

use App\Mahasiswa;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    public function index(){
        $no = 1;
        $mahasiswas = Mahasiswa::select('mahasiswas.*','pendaftarans.*','users.*')
                      ->join('pendaftarans', 'pendaftarans.id','=','mahasiswas.pendaftaran_id')
                      ->join('users', 'users.id', '=', 'mahasiswas.user_id')
                      ->where('pendaftarans.status', '=',1)
                      ->get();
        ;
        return view('kabag.mahasiswa.index', compact('no','mahasiswas'));
    }
}
