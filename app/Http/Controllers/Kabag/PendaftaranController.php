<?php

namespace App\Http\Controllers\Kabag;

use App\User;
use App\Mahasiswa;
use App\Pendaftaran;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PendaftaranController extends Controller
{
    public function index(){
        $no = 1;
        $pendaftarans = Mahasiswa::all();
        return view('kabag.pendaftaran.index', compact('no','pendaftarans'));
    }

    public function detail($id){
        $pendaftaran = Mahasiswa::where('id',$id)->first();
        return view('kabag.pendaftaran.detail', compact('pendaftaran'));
    }
}
