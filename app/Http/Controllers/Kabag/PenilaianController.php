<?php

namespace App\Http\Controllers\Kabag;

use App\User;
use App\Mahasiswa;
use App\Penilaian;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PenilaianController extends Controller
{
    public function dataMhs(){
        $no = 1;
        $mahasiswas = Mahasiswa::select('mahasiswas.*','pendaftarans.*','users.*')
                      ->join('pendaftarans', 'pendaftarans.id','=','mahasiswas.pendaftaran_id')
                      ->join('users', 'users.id', '=', 'mahasiswas.user_id')
                      ->where('pendaftarans.status', '=',1)
                      ->get();
        return view('kabag.penilaian.index', compact('no','mahasiswas'));
    }

    public function nilaiMhs($id){
        $mhs = Mahasiswa::where('user_id', $id)->first();

        $penilaian = Penilaian::where('mahasiswa_id', $mhs->id)->first();
        if ($penilaian == null) {
            $p = FALSE;
        }else{
            $p = TRUE;
        }
        return view('kabag.penilaian.nilai', compact('mhs', 'penilaian', 'p'));
    }
}
