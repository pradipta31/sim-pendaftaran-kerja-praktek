<?php

namespace App\Http\Controllers\Mahasiswa;

use Auth;
use Validator;
use App\Mahasiswa;
use App\Jurnal;
use App\Absensi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class JurnalController extends Controller
{
    public function tambahJurnal(){
        $mhs = Mahasiswa::where('user_id', Auth::user()->id)->first();
        $date_now = date('Y-m-d');
        // dd($date_now);
        $jurnal = Jurnal::where('tanggal_kegiatan', $date_now)
                        ->where('mahasiswa_id', $mhs->id)
                        ->first();
        if ($jurnal != null) {
            return view('mahasiswa.jurnal.blank');
        }else{
            return view('mahasiswa.jurnal.create');
        }
    }

    public function simpanJurnal(Request $r){
        $validator = Validator::make($r->all(), [
            'keterangan' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $mhs = Mahasiswa::where('user_id', Auth::user()->id)->first();
            $jurnals = Jurnal::create([
                'mahasiswa_id' => $mhs->id,
                'tanggal_kegiatan' => date('Y-m-d'),
                'keterangan' => $r->keterangan,
                'status' => 2
            ]);

            $absent = Absensi::create([
                'mahasiswa_id' => $mhs->id,
                'jurnal_id' => $jurnals->id,
                'tanggal' => date('Y-m-d')
            ]);

            toastSuccess('Jurnal dan absensi anda berhasil dikirim, mohon tunggu divalidasi oleh petugas!');
            return redirect(url('mahasiswa/jurnal'));
        }
    }

    public function indexJurnal(){
        $no = 1;
        $mahasiswa = Mahasiswa::where('user_id',Auth::user()->id)->first();
        $jurnals = Jurnal::where('mahasiswa_id',$mahasiswa->id)->get();
        // dd($mahasiswa);
        return view('mahasiswa.jurnal.index', compact('no','jurnals'));
    }

    public function indexAbsen(){
        $no = 1;
        $mahasiswa = Mahasiswa::where('user_id',Auth::user()->id)->first();
        $absents = Absensi::where('mahasiswa_id',$mahasiswa->id)->get();
        return view('mahasiswa.jurnal.absen', compact('no','absents'));
    }
}
