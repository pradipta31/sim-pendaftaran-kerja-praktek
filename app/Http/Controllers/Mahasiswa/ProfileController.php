<?php

namespace App\Http\Controllers\Mahasiswa;

use Hash;
use Validator;
use Image;
use Auth;
use App\User;
use App\Mahasiswa;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(){
        $mahasiswa = Mahasiswa::where('user_id', Auth::user()->id)->first();
        return view('mahasiswa.profile.index', compact('mahasiswa'));
    }

    public function updateAvatar(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $mhs = Mahasiswa::where('id', $id)->first();
            $user = User::where('id',$mhs->user_id)->first();

            $avatar = $r->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            
            if ($user->avatar != null) {
                unlink(public_path('/images/ava/').$user->avatar);
            }

            Image::make($avatar)->save(public_path('/images/ava/'.$filename));
            
            $user->update([
                'avatar' => $filename
            ]);

            toastSuccess('Update foto profil berhasil');
            return redirect()->back();
        }
    }

    public function updateProfile(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'nama' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->password == null) {
                $mhs = Mahasiswa::where('id',$id)->first();
                $user = User::where('id', $mhs->user_id)->first();
                $mhs->update([
                    'alamat' => $r->alamat,
                    'no_telp' => $r->no_telp
                ]);

                $user->update([
                    'nama' => $r->nama,
                    'email' => $r->email
                ]);

                toastSuccess('Update data profile berhasil!');
                return redirect()->back();
            } else {
                if ($r->password != $r->confirmation_password) {
                    toastError('Password tidak sama!');
                    return redirect()->back();
                } else {
                    $mhs = Mahasiswa::where('id',$id)->first();
                    $user = User::where('id', $mhs->user_id)->first();
                    $mhs->update([
                        'alamat' => $r->alamat,
                        'no_telp' => $r->no_telp
                    ]);

                    $user->update([
                        'nama' => $r->nama,
                        'email' => $r->email,
                        'password' => Hash::make($r->password)
                    ]);

                    toastSuccess('Update data profile berhasil!');
                    return redirect()->back();
                }
                
            }
            
        }
    }
}
