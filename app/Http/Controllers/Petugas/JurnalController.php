<?php

namespace App\Http\Controllers\Petugas;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Auth;
use PDF;
use App\Mahasiswa;
use App\Jurnal;
use App\Absensi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class JurnalController extends Controller
{
    public function showMhs(){
        $no = 1;
        $mahasiswas = Mahasiswa::select('mahasiswas.*','pendaftarans.*','users.*')
                      ->join('pendaftarans', 'pendaftarans.id','=','mahasiswas.pendaftaran_id')
                      ->join('users', 'users.id', '=', 'mahasiswas.user_id')
                      ->where('pendaftarans.status', '=',1)
                      ->get();
        return view('petugas.jurnal.mahasiswa', compact('no','mahasiswas'));
    }

    public function indexJurnal($id){
        $no = 1;
        $mhs = Mahasiswa::where('user_id', $id)->first();
        $jurnals = Jurnal::where('mahasiswa_id', $mhs->id)
        ->orderBy('created_at', 'ASC')
        ->get();
        return view('petugas.jurnal.jurnal', compact('mhs','no', 'jurnals'));
    }

    public function indexAbsen($id){
        $no = 1;
        $mhs = Mahasiswa::where('user_id', $id)->first();
        $absents = Absensi::where('mahasiswa_id', $mhs->id)->get();
        return view('petugas.jurnal.absen', compact('mhs','no', 'absents'));
    }

    public function validasiJurnal($id){
        $jurnal = Jurnal::where('id', $id)->first();
        $jurnal->update([
            'admin_id' => Auth::user()->id,
            'tanggal_validasi' => date('Y-m-d'),
            'status' => 1
        ]);

        $absen = Absensi::where('jurnal_id', $jurnal->id)->update([
            'admin_id' => Auth::user()->id,
            'status' => 'Hadir'
        ]);

        toastSuccess('Jurnal berhasil di validasi!');
        return redirect()->back();
    }

    public function tolakJurnal($id){
        $jurnal = Jurnal::where('id', $id)->first();
        $jurnal->update([
            'admin_id' => Auth::user()->id,
            'tanggal_validasi' => date('Y-m-d'),
            'status' => 0
        ]);

        $absen = Absensi::where('jurnal_id', $jurnal->id)->update([
            'admin_id' => Auth::user()->id,
            'status' => 'Tidak Hadir'
        ]);

        toastSuccess('Jurnal telah ditolak!');
        return redirect()->back();
    }

    // FUNCTION UNTUK CETAK JURNAL DAN ABSEN

    public function cetakJurnal($id){
        $mhs = Jurnal::where('mahasiswa_id', $id)->first();
        $jurnal = Jurnal::where('mahasiswa_id', $id)->where('status',1)->get();
        $pdf = PDF::loadview('petugas.jurnal.jurnal_pdf',['mhs'=>$mhs,'jurnal'=>$jurnal]);
	    return $pdf->stream('jurnal-mahasiswa-kp');
    }

    public function cetakAbsen($id){
        $mhs = Absensi::where('mahasiswa_id', $id)->first();
        $absen = Absensi::where('mahasiswa_id', $id)->where('status','!=',NULL)->get();
        $pdf = PDF::loadview('petugas.jurnal.absen_pdf',['mhs'=>$mhs,'absen'=>$absen]);
	    return $pdf->stream('absen-mahasiswa-kp');
    }

    public function cetakJurnalLL($id){
        $spreadsheet = new Spreadsheet();
        $mahasiswa = Mahasiswa::where('id',$id)->first();
        $jurnals = Jurnal::where('mahasiswa_id',$id)->where('status', 1)->get();

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'JURNAL MAHASISWA: '.$mahasiswa->user->nama);
        $sheet->setCellValue('A3', 'No');
        $sheet->setCellValue('B3', 'Tanggal Kegiatan');
        $sheet->setCellValue('C3', 'Tanggal Validasi');
        $sheet->setCellValue('D3', 'Keterangan');
        $sheet->setCellValue('E3', 'Petugas');
        $sheet->setCellValue('F3', 'Status');

        $row = 4;
        $nomor = 1;
        foreach ($jurnals as $jurnal) {
            $status = '';
            if ($jurnal->status == 0) {
                $status = 'Dalam Proses';
            }elseif ($jurnal->status == 1) {
                $status = 'Divalidasi';
            }elseif ($jurnal->status == 2) {
                $status = 'Ditolak';
            }
            $sheet->setCellValue('A'.$row,$nomor++);
            $sheet->setCellValue('B'.$row,$jurnal->tanggal_kegiatan);
            $sheet->setCellValue('C'.$row,$jurnal->tanggal_validasi);
            $sheet->setCellValue('D'.$row,$jurnal->keterangan);
            $sheet->setCellValue('E'.$row,$jurnal->admin->user->nama);
            $sheet->setCellValue('F'.$row,$status);
            $row++;
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save('Data Jurnal '.$mahasiswa->user->nama.'.xlsx');
        return response()->download(public_path('Data Jurnal '.$mahasiswa->user->nama.'.xlsx'))->deleteFileAfterSend();
    }

    public function cetakAbsennn($id){
        $spreadsheet = new Spreadsheet();
        $mahasiswa = Mahasiswa::where('id',$id)->first();
        $absents = Absensi::where('mahasiswa_id',$id)->get();

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'ABSEN MAHASISWA : '.$mahasiswa->user->nama);
        $sheet->setCellValue('A3', 'No');
        $sheet->setCellValue('B3', 'Tanggal');
        $sheet->setCellValue('C3', 'Tanggal Jurnal');
        $sheet->setCellValue('D3', 'Petugas');
        $sheet->setCellValue('E3', 'Status');

        $row = 4;
        $nomor = 1;
        foreach ($absents as $absen) {
            $sheet->setCellValue('A'.$row,$nomor++);
            $sheet->setCellValue('B'.$row,date('d-m-Y', strtotime($absen->tanggal)));
            $sheet->setCellValue('C'.$row,$absen->jurnal->tanggal_kegiatan);
            $sheet->setCellValue('D'.$row,$absen->admin->user->nama);
            $sheet->setCellValue('E'.$row,$absen->status);
            $row++;
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save('Data Absen '.$mahasiswa->user->nama.'.xlsx');
        return response()->download(public_path('Data Absen '.$mahasiswa->user->nama.'.xlsx'))->deleteFileAfterSend();
    }

}
