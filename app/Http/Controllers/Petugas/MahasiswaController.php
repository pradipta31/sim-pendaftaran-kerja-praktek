<?php

namespace App\Http\Controllers\Petugas;

use Hash;
use Validator;
use App\User;
use App\Mahasiswa;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    public function index(){
        $no = 1;
        $mahasiswas = Mahasiswa::select('mahasiswas.*','pendaftarans.*','users.*')
                      ->join('pendaftarans', 'pendaftarans.id','=','mahasiswas.pendaftaran_id')
                      ->join('users', 'users.id', '=', 'mahasiswas.user_id')
                      ->where('pendaftarans.status', '=',1)
                      ->get();
        ;
        return view('petugas.mahasiswa.index', compact('no','mahasiswas'));
    }

    public function edit($id){
        $mahasiswa = Mahasiswa::select('mahasiswas.*','pendaftarans.*','users.*')
                    ->join('pendaftarans', 'pendaftarans.id','=','mahasiswas.pendaftaran_id')
                    ->join('users', 'users.id', '=', 'mahasiswas.user_id')
                    ->where('users.id', '=', $id)
                    ->first();
        return view('petugas.mahasiswa.edit', compact('mahasiswa'));
    }

    public function update(Request $r, $id){
        $validator = Validator::make($r->all(),[
            'nama' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required',
            'status' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        } else {
            $mahasiswa = User::where('id', $id)->update([
                'nama' => $r->nama,
                'status' => $r->status
            ]);

            $mhs = Mahasiswa::where('user_id', $id)->update([
                'alamat' => $r->alamat,
                'no_telp' => $r->no_telp,
                'status' => $r->status
            ]);

            toastSuccess('Berhasil memperbaharui data mahasiswa');
            return redirect(url('petugas/mahasiswa'));
        }
        
    }

    public function chPass(Request $r, $id){
        $user = User::where('id',$id)->update([
            'password' => Hash::make($r->password)
        ]);
        toastSuccess('Password berhasil diubah!');
        return redirect()->back();
    }
}
