<?php

namespace App\Http\Controllers\Petugas;

use Validator;
use Hash;
use Auth;
use App\User;
use App\Mahasiswa;
use App\Pendaftaran;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PendaftaranController extends Controller
{
    public function index(){
        $no = 1;
        $pendaftarans = Mahasiswa::all();
        return view('petugas.pendaftaran.index', compact('no','pendaftarans'));
    }

    public function detail($id){
        $pendaftaran = Mahasiswa::where('id',$id)->first();
        return view('petugas.pendaftaran.detail', compact('pendaftaran'));
    }

    public function download($id){
        return response()->file(base_path('storage/app/public/berkas/pendaftaran/'.$id));
    }

    public function setujuPendaftaran(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'tgl_mulai' => 'required',
            'tgl_selesai' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $mulai = date_create($r->tgl_mulai);
            $selesai = date_create($r->tgl_selesai);
            $diff = date_diff( $mulai, $selesai );
            
            if ($diff->days < 90) {
                toastError('Kerja praktek harus dilaksanakan 90 hari(3 bulan) penuh');
                return redirect()->back()->withInput();
            }else{
                $mahasiswa = Mahasiswa::where('id', $id)->first();
                $pendaftaran = Pendaftaran::where('id',$mahasiswa->pendaftaran_id)->first();
                $user = User::where('id', $mahasiswa->user_id)->first();
                $length = 6;    
                $pass = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,$length);
                // echo $pass;
                $pendaftaran->update([
                    'status' => 1
                ]);

                $user->update([
                    'password' => Hash::make($pass),
                    'status' => 1
                ]);

                $mahasiswa->update([
                    'admin_id' => Auth::user()->id,
                    'tgl_mulai' => $r->tgl_mulai,
                    'tgl_selesai' => $r->tgl_selesai,
                    'status' => 1
                ]);

                try{
                    \Mail::send('petugas.pendaftaran.email-terima', [
                        'nama' => $user->nama,
                        'email' => $user->email,
                        'password' => $pass,
                        'bidang' => $pendaftaran->bidang,
                        'tgl_mulai' => $r->tgl_mulai,
                        'tgl_selesai' => $r->tgl_selesai
                    ], function ($message) use ($user)
                    {
                        $message->subject('Hi! '.$user->nama);
                        $message->from('pradiptadipta31@gmail.com', 'Manajemen Mahasiswa Kerja Praktek');
                        $message->to($user->email);
                    });
                    toastr()->success('Pendaftaran mahasiswa berhasil disetujui. Email berhasil dikirim ke '.$user->email, '', ['timeOut' => 5000]);
                    return redirect()->back();
                }
                catch (Exception $e){
                    return response (['status' => false,'errors' => $e->getMessage()]);
                }
            }
        }
    }

    public function tolakPendaftaran(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'keterangan' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        } else {
            $mahasiswa = Mahasiswa::where('id', $id)->first();
            $pendaftaran = Pendaftaran::where('id',$mahasiswa->pendaftaran_id)->first();
            $user = User::where('id', $mahasiswa->user_id)->first();
            $pendaftaran->update([
                'keterangan' => $r->keterangan,
                'status' => 0
            ]);

            $user->update([
                'status' => 0
            ]);

            $mahasiswa->update([
                'admin_id' => Auth::user()->id,
                'status' => 0
            ]);

            try{
                \Mail::send('petugas.pendaftaran.email-tolak', [
                    'nama' => $user->nama,
                    'email' => $user->email,
                    'bidang' => $pendaftaran->bidang,
                    'keterangan' => $r->keterangan
                ], function ($message) use ($user)
                {
                    $message->subject('Hi! '.$user->nama);
                    $message->from('pradiptadipta31@gmail.com', 'Manajemen Mahasiswa Kerja Praktek');
                    $message->to($user->email);
                });
                toastr()->warning('Pendaftaran mahasiswa telah ditolak. Email berhasil dikirim ke '.$user->email, '', ['timeOut' => 5000]);
                return redirect()->back();
            }
            catch (Exception $e){
                return response (['status' => false,'errors' => $e->getMessage()]);
            }
        }
        
    }
}
