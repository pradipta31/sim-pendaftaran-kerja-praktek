<?php

namespace App\Http\Controllers\Petugas;

use Validator;
use Auth;
use Hash;
use App\User;
use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PenggunaController extends Controller
{
    public function tambahPengguna(){
        $kabag = User::where('level', 2)->count();
        return view('petugas.pengguna.create', compact('kabag'));
    }

    public function indexPengguna(){
        $no = 1;
        $users = Admin::all();
        return view('petugas.pengguna.index', compact('no', 'users'));
    }

    public function simpanPengguna(Request $r){
        $validator = Validator::make($r->all(), [
            'nip' => 'required|numeric|unique:admins',
            'nama' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
            'level' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $users = User::create([
                'nama' => $r->nama,
                'email' => $r->email,
                'password' => Hash::make($r->password),
                'level' => $r->level,
                'status' => 1
            ]);

            $admins = Admin::create([
                'user_id' => $users->id,
                'nip' => $r->nip
            ]);

            toastSuccess('Pengguna baru berhasil ditambahkan!');
            return redirect('petugas/pengguna');
        }
    }

    public function editPengguna($id){
        $user = Admin::where('user_id', $id)->first();
        // dd($user);
        return view('petugas.pengguna.edit', compact('user'));
    }

    public function updatePengguna(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'nip' => 'required|numeric',
            'nama' => 'required',
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $admins = Admin::where('user_id',$id)->update([
                'nip' => $r->nip
            ]);

            if ($r->password == null) {
                $users = User::where('id',$id)->update([
                    'nama' => $r->nama,
                    'email' => $r->email,
                    'status' => $r->status
                ]);
            }else{
                $users = User::where('id',$id)->update([
                    'nama' => $r->nama,
                    'email' => $r->email,
                    'password' => Hash::make($r->password),
                    'level' => $r->level,
                    'status' => $r->status
                ]);
            }

            toastSuccess('Pengguna baru berhasil ditambahkan!');
            return redirect('petugas/pengguna');
        }
    }
}
