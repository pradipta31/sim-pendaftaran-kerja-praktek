<?php

namespace App\Http\Controllers\Petugas;

use Auth;
use Validator;
use App\Announcement;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PengumumanController extends Controller
{
    public function tambahPengumuman(){
        return view('petugas.pengumuman.create');
    }

    public function simpanPengumuman(Request $r){
        $validator = Validator::make($r->all(), [
            'judul' => 'required',
            'isi' => 'required',
            'tgl_mulai' => 'required',
            'tgl_akhir' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $pengumuman = Announcement::create([
                'admin_id' => Auth::user()->id,
                'judul' => $r->judul,
                'tgl_mulai' => $r->tgl_mulai,
                'tgl_akhir' => $r->tgl_akhir,
                'isi' => $r->isi,
                'status' => $r->status
            ]);
            toastSuccess('Pengumuman baru telah di simpan');
            return redirect(url('petugas/pengumuman'));
        }
    }

    public function indexPengumuman(){
        $no = 1;
        $pengumumans = Announcement::all();
        return view('petugas.pengumuman.index', compact('no','pengumumans'));
    }

    public function editPengumuman($id){
        $pengumuman = Announcement::where('id',$id)->first();
        return view('petugas.pengumuman.edit', compact('pengumuman'));
    }

    public function updatePengumuman(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'judul' => 'required',
            'isi' => 'required',
            'tgl_mulai' => 'required',
            'tgl_akhir' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $pengumuman = Announcement::where('id',$id)->update([
                'judul' => $r->judul,
                'isi' => $r->isi,
                'tgl_mulai' => $r->tgl_mulai,
                'tgl_akhir' => $r->tgl_akhir,
                'status' => $r->status
            ]);
            toastSuccess('Pengumuman baru telah di simpan');
            return redirect(url('petugas/pengumuman'));
        }
    }
}
