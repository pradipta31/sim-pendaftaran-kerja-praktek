<?php

namespace App\Http\Controllers\Petugas;

use PDF;
use Auth;
use Validator;
use App\User;
use App\Admin;
use App\Mahasiswa;
use App\Penilaian;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PenilaianController extends Controller
{
    public function dataMhs(){
        $no = 1;
        $mahasiswas = Mahasiswa::select('mahasiswas.*','pendaftarans.*','users.*')
                      ->join('pendaftarans', 'pendaftarans.id','=','mahasiswas.pendaftaran_id')
                      ->join('users', 'users.id', '=', 'mahasiswas.user_id')
                      ->where('pendaftarans.status', '=',1)
                      ->get();
        return view('petugas.penilaian.index', compact('no','mahasiswas'));
    }

    public function nilaiMhs($id){
        $mhs = Mahasiswa::where('user_id', $id)->first();

        $penilaian = Penilaian::where('mahasiswa_id', $mhs->id)->first();
        if ($penilaian == null) {
            $p = FALSE;
        }else{
            $p = TRUE;
        }
        return view('petugas.penilaian.nilai', compact('mhs', 'penilaian', 'p'));
    }

    public function tambahNilai($id){
        $mahasiswa = Mahasiswa::where('id',$id)->first();
        return view('petugas.penilaian.tambah', compact('mahasiswa'));
    }

    public function simpanNilai(Request $r){
        $validator = Validator::make($r->all(),[
            'mahasiswa_id' => 'required',
            'adaptasi' => 'required|numeric',
            'kerjasama' => 'required|numeric',
            'kesungguhan' => 'required|numeric',
            'kehadiran' => 'required|numeric',
            'tanggung_jawab' => 'required|numeric',
            'pemahaman_tugas' => 'required|numeric',
            'inisiatif' => 'required|numeric',
            'total' => 'required|numeric',
            'rata' => 'required|numeric',
            'masukan' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        } else {
            $mahasiswa = Mahasiswa::where('id', $r->mahasiswa_id)->first();
            $admin = Admin::where('user_id', Auth::user()->id)->first();
            $penilaian = Penilaian::create([
                'admin_id' => $admin->id,
                'mahasiswa_id' => $r->mahasiswa_id,
                'adaptasi' => $r->adaptasi,
                'kerjasama' => $r->kerjasama,
                'kesungguhan' => $r->kesungguhan,
                'kehadiran' => $r->kehadiran,
                'tanggung_jawab' => $r->tanggung_jawab,
                'pemahaman_tugas' => $r->pemahaman_tugas,
                'inisiatif' => $r->inisiatif,
                'total_nilai' => $r->total,
                'rata_rata' => $r->rata,
                'tanggal' => date('Y-m-d'),
                'masukan' => $r->masukan
            ]);
            toastSuccess('Penilaian mahasiswa '.$mahasiswa->user->nama.' Berhasil ditambahkan!');
            return redirect(url('petugas/penilaian/'.$mahasiswa->user_id.'/mahasiswa'));
        }
        
    }

    public function editNilai($id){
        $penilaian = Penilaian::where('mahasiswa_id',$id)->first();
        return view('petugas.penilaian.edit', compact('penilaian'));
    }

    public function updateNilai(Request $r, $id){
        $validator = Validator::make($r->all(),[
            'mahasiswa_id' => 'required',
            'adaptasi' => 'required|numeric',
            'kerjasama' => 'required|numeric',
            'kesungguhan' => 'required|numeric',
            'kehadiran' => 'required|numeric',
            'tanggung_jawab' => 'required|numeric',
            'pemahaman_tugas' => 'required|numeric',
            'inisiatif' => 'required|numeric',
            'total' => 'required|numeric',
            'rata' => 'required|numeric',
            'masukan' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        } else {
            $mahasiswa = Mahasiswa::where('id', $r->mahasiswa_id)->first();
            $penilaian = Penilaian::where('id',$id)->update([
                'adaptasi' => $r->adaptasi,
                'kerjasama' => $r->kerjasama,
                'kesungguhan' => $r->kesungguhan,
                'kehadiran' => $r->kehadiran,
                'tanggung_jawab' => $r->tanggung_jawab,
                'pemahaman_tugas' => $r->pemahaman_tugas,
                'inisiatif' => $r->inisiatif,
                'total_nilai' => $r->total,
                'rata_rata' => $r->rata,
                'masukan' => $r->masukan
            ]);
            toastSuccess('Penilaian mahasiswa '.$mahasiswa->user->nama.' Berhasil ditambahkan!');
            return redirect(url('petugas/penilaian/'.$mahasiswa->user_id.'/mahasiswa'));
        }
    }

    public function cetakPdf($id){
        $penilaian = Penilaian::where('mahasiswa_id', $id)->first();
        $pdf = PDF::loadview('petugas.penilaian.print',['penilaian'=>$penilaian]);
	    return $pdf->stream('penilaian-mahasiswa-kp');
    }
}
