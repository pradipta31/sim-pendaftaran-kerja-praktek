<?php

namespace App\Http\Controllers\Petugas;

use Hash;
use Validator;
use Image;
use Auth;
use App\User;
use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(){
        $admin = Admin::where('user_id', Auth::user()->id)->first();
        return view('petugas.profile.index', compact('admin'));
    }

    public function updateAvatar(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $user = User::where('id',$id)->first();

            $avatar = $r->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            
            if ($user->avatar != null) {
                unlink(public_path('/images/ava/').$user->avatar);
            }

            Image::make($avatar)->save(public_path('/images/ava/'.$filename));
            
            $user->update([
                'avatar' => $filename
            ]);

            toastSuccess('Update foto profil berhasil');
            return redirect()->back();
        }
    }

    public function updateProfile(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'nama' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'no_hp' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->password == null) {
                $user = User::where('id', $id)->first();
                $admin = Admin::where('user_id', $user->id)->first();
                $admin->update([
                    'alamat' => $r->alamat,
                    'no_hp' => $r->no_hp,
                    'tanggal_lahir' => $r->tanggal_lahir
                ]);

                $user->update([
                    'nama' => $r->nama,
                    'email' => $r->email
                ]);

                toastSuccess('Update data profile berhasil!');
                return redirect()->back();
            } else {
                if ($r->password != $r->confirmation_password) {
                    toastError('Password tidak sama!');
                    return redirect()->back();
                } else {
                    $user = User::where('id', $mhs->user_id)->first();
                    $admin->update([
                        'alamat' => $r->alamat,
                        'no_hp' => $r->no_hp,
                        'tanggal_lahir' => $r->tanggal_lahir
                    ]);

                    $user->update([
                        'nama' => $r->nama,
                        'email' => $r->email,
                        'password' => Hash::make($r->password)
                    ]);

                    toastSuccess('Update data profile berhasil!');
                    return redirect()->back();
                }
                
            }
            
        }
    }
}
