<?php

namespace App\Http\Controllers;

use Validator;
use App\User;
use App\Mahasiswa;
use App\Pendaftaran;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index(){
        return view('mahasiswa.pendaftaran');
    }

    public function simpanPendaftaran(Request $r){
        $validator = Validator::make($r->all(), [
            'nim' => 'required|numeric|unique:mahasiswas',
            'nama' => 'required',
            'email' => 'required',
            'nomor_surat' => 'required',
            'perihal' => 'required',
            'bidang' => 'required',
            'asal_kampus' => 'required',
            'program_studi' => 'required',
            'jenjang_studi' => 'required',
            'no_telp' => 'required',
            'document' => 'required|mimes:pdf|max:10000'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $file = $r->file('document');
            $document = time().'.'.$file->getClientOriginalExtension();
            $path_url = 'berkas/pendaftaran';
            $file->storeAs($path_url,$document,'public');

            $pend = Pendaftaran::create([
                'nomor_surat' => $r->nomor_surat,
                'perihal' => $r->perihal,
                'bidang' => $r->bidang,
                'asal_kampus' => $r->asal_kampus,
                'program_studi' => $r->program_studi,
                'jenjang_studi' => $r->jenjang_studi,
                'document' => $document,
                'keterangan' => '-',
                'status' => 2,
                'tanggal_input' => date('Y-m-d')
            ]);

            $users = User::create([
                'nama' => $r->nama,
                'email' => $r->email,
                'level' => 3,
                'status' => 0
            ]);

            $mhs = Mahasiswa::create([
                'user_id' => $users->id,
                'pendaftaran_id' => $pend->id,
                'nim' => $r->nim,
                'no_telp' => $r->no_telp,
                'status' => 0,
            ]);

            toastSuccess('Pendaftaran berhasil!');
            return redirect(url('registrasi/berhasil'));
        }
    }

    public function berhasil(){
        return view('mahasiswa.pendaftaran-berhasil');
    }
}
