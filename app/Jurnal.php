<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurnal extends Model
{
    protected $fillable = [
        'admin_id',
        'mahasiswa_id',
        'tanggal_kegiatan',
        'tanggal_validasi',
        'keterangan',
        'status'
    ];

    public function admin(){
        return $this->belongsTo('App\Admin');
    }

    public function mahasiswa(){
        return $this->belongsTo('App\Mahasiswa');
    }

    public $timestamps = true;
}
