<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $fillable = [
        'user_id',
        'admin_id',
        'pendaftaran_id',
        'nim',
        'alamat',
        'no_telp',
        'tgl_mulai',
        'tgl_selesai',
        'status'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function admin(){
        return $this->belongsTo('App\Admin');
    }

    public function pendaftaran(){
        return $this->belongsTo('App\Pendaftaran');
    }

    public $timestamps = true;
}
