<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendaftaran extends Model
{
    protected $fillable = [
        'nomor_surat',
        'perihal',
        'bidang',
        'asal_kampus',
        'program_studi',
        'jenjang_studi',
        'document',
        'keterangan',
        'status',
        'tanggal_input'
    ];

    public $timestamps = true;
}
