<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penilaian extends Model
{
    protected $fillable = [
        'admin_id',
        'mahasiswa_id',
        'adaptasi',
        'kerjasama',
        'kesungguhan',
        'kehadiran',
        'tanggung_jawab',
        'pemahaman_tugas',
        'inisiatif',
        'total_nilai',
        'rata_rata',
        'tanggal',
        'masukan'
    ];

    public function admin(){
        return $this->belongsTo('App\Admin');
    }

    public function mahasiswa(){
        return $this->belongsTo('App\Mahasiswa');
    }

    public $timestamps = true;
}
