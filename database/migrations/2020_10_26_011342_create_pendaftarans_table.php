<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendaftaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendaftarans', function (Blueprint $table) {
            $table->id();
            $table->string('nomor_surat', 50);
            $table->string('perihal', 50);
            $table->string('bidang', 50);
            $table->string('asal_kampus', 50);
            $table->string('program_studi', 50);
            $table->string('jenjang_studi', 50);
            $table->string('document');
            $table->text('keterangan');
            $table->integer('status');
            $table->date('tanggal_input');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendaftarans');
    }
}
