<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenilaiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaians', function (Blueprint $table) {
            $table->id();
            $table->foreignId('admin_id')->nullable()->constrained();
            $table->foreignId('mahasiswa_id')->constrained();
            $table->integer('adaptasi');
            $table->integer('kerjasama');
            $table->integer('kesungguhan');
            $table->integer('kehadiran');
            $table->integer('tanggung_jawab');
            $table->integer('pemahaman_tugas');
            $table->integer('inisiatif');
            $table->integer('total_nilai');
            $table->double('rata_rata');
            $table->text('masukan');
            $table->date('tanggal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilaians');
    }
}
