<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mahasiswa Kerja Praktek KPU Prov. Bali | Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{asset('backend/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @toastr_css
    <link rel="stylesheet" href="{{asset('backend/bower_components/Ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/plugins/iCheck/square/blue.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box" style="margin-top: 180px">
    <div class="login-logo">
        <a href="{{url('login')}}"><b>Mahasiswa Kerja Praktek</b> Login</a>
    </div>
    <div class="login-box-body">
        <img src="{{asset('images/logo/logo-angkasa-1.png')}}" alt="" class="img-responsive">
        <br>
        <p class="login-box-msg">Silahkan login terlebih dahulu</p>
        <form action="{{route('login')}}" method="POST">
        @csrf
            <div class="form-group has-feedback">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                name="email" value="{{old('email')}}" placeholder="Masukan Email" required autofocus>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>Email/password salah atau akun sudah di nonaktifkan!</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Password" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>Password Tidak boleh kosong!</strong>
                    </span>
                @enderror
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block" onclick="loginBtn(this);">
                        <i class="fa fa-sign-in"></i>
                    Sign In
                    </button>
                </div>
                
                
            </div>
            <div class="social-auth-links text-center">
                <p>- OR -</p>                
                <a href="{{url('registrasi')}}" class="btn btn-block btn-social btn-facebook"><i class="fa fa-cloud-upload"></i>Pendaftaran Mahasiswa Kerja Praktek</a>
                <!-- USING MODAL HERE -->
                <a href="javascript:void(0);" class="btn btn-block btn-social btn-google" data-toggle="modal" data-target="#alurPendaftaran">
                    <i class="fa fa-child"></i>
                    Alur Pendaftaran
                </a>
                <div class="modal fade" id="alurPendaftaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3 class="modal-title" id="exampleModalLabel">Alur Pendaftaran Mahasiswa Kerja Praktek</h3>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <a href="{{url('images/alur_pendaftaran.jpeg')}}" target="_blank" rel="noopener noreferrer">
                                    <img src="{{asset('images/alur_pendaftaran.jpeg')}}" alt="" class="img-fluid img-responsive">
                                </a>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                <a download="Alur-Pendaftaran.jpeg" href="{{asset('images/alur_pendaftaran.jpeg')}}" class="btn btn-primary">
                                    <i class="fa fa-cloud-download"></i>
                                    Download Gambar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <small>NB: Jika lupa password login harap untuk menghubungi Petugas KPU.</small>
            </div>
        </form>
    </div>
</div>
@jquery
@toastr_js
@toastr_render
<script src="{{asset('backend/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('backend/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('backend/plugins/iCheck/icheck.min.js')}}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%'
    });
  });
  function loginBtn(d){
    d.innerHTML = '<i class="fa fa-spinner fa-spin"></i>';
  }
</script>
</body>
</html>
