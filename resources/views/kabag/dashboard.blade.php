@extends('layouts.master',['activeMenu' => 'dashboard'])
@section('title','Dashboard')
@section('breadcrumb', 'Dashboard')
@section('detail_breadcrumb', 'Control Panel')
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{$validasi_jurnal}}</h3>
        
                        <p>Jurnal Belum Di Validasi</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-warning"></i>
                    </div>
                    <a href="{{url('kabag/jurnal')}}" class="small-box-footer">More Info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{$mahasiswa}}</h3>
        
                        <p>Mahasiswa Aktif</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-check"></i>
                    </div>
                    <a href="{{url('kabag/mahasiswa')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{$pendaftaran}}</h3>
        
                        <p>Data Pendaftaran</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{{url('kabag/pendaftaran')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>{{$bidang_1}}</h3>
        
                        <p>Bagian Program, Data Organisasi dan SDM</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-purple">
                    <div class="inner">
                        <h3>{{$bidang_2}}</h3>
        
                        <p>Bagian Keuangan, Umum dan Logistik</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$bidang_3}}</h3>
        
                        <p>Bagian Hukum, Teknik dan Hupmas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                
                </div>
            </div>

            <div class="col-lg-12 col-xs-12 col-md-12">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Selamat datang {{Auth::user()->nama}}</h4>
                    Pada halaman dashboard anda dapat melihat beberapa informasi mengenai website anda.
                </div>

                <ul class="timeline">

                    <!-- timeline time label -->
                    <li class="time-label">
                        <span class="bg-red">
                            PENGUMUMAN
                        </span>
                    </li>
                    <!-- /.timeline-label -->
                
                    <!-- timeline item -->
                    @foreach ($pengumumans as $pengumuman)
                        <li>
                            <!-- timeline icon -->
                            <i class="fa fa-bell bg-blue"></i>
                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> {{date('d M, Y H:i:s', strtotime($pengumuman->created_at))}}</span>
                    
                                <h2 class="timeline-header"><a href="#">{{$pengumuman->judul}}</a> {{date('d-m-Y',strtotime($pengumuman->tgl_mulai))}} - {{date('d-m-Y',strtotime($pengumuman->tgl_akhir))}}</h2>
                    
                                <div class="timeline-body">
                                    {!! $pengumuman->isi !!}
                                </div>
                    
                                <div class="timeline-footer">
                                    <h5>
                                        PETUGAS KERJA PRAKTEK
                                    </h5>
                                </div>
                            </div>
                        </li>
                    @endforeach
                
                </ul>
            </div>
        </div>
    </section>
@endsection

@section('js')
    
@endsection
