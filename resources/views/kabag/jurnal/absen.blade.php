@extends('layouts.master',['activeMenu' => 'jurnal'])
@section('title','Data Absen')
@section('breadcrumb', 'Data Absen Kegiatan')
@section('detail_breadcrumb', 'Manajemen Data Absen Kegiatan')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tabelAbsen" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Petugas</th>
                                        <th>Tanggal Kegiatan</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($absents as $absent)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>
                                                @if ($absent->admin_id == null)
                                                    -
                                                @else
                                                    {{$absent->admin->user->nama}}
                                                @endif
                                            </td>
                                            <td>
                                                {{date('d-m-Y', strtotime($absent->tanggal))}}
                                            </td>
                                            <td>
                                                @if ($absent->status == null)
                                                    <span class="label label-warning">Belum di validasi</span>
                                                @else
                                                    @if ($absent->status == 'Hadir')
                                                        <span class="label label-primary">Hadir</span>
                                                    @else
                                                        <span class="label label-danger">Tidak Hadir</span>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelAbsen').dataTable()
        });

        function editUserModal(){
            $('#editUser').modal('show');
        }
    </script>
@endsection
