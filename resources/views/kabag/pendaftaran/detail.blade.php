@extends('layouts.master',['activeMenu' => 'pendaftaran'])
@section('title','Data Pendaftaran')
@section('breadcrumb', 'Data Pendaftaran')
@section('detail_breadcrumb', 'Data Pendaftaran Mahasiswa Kerja Praktek')
@section('content')
    @include('layouts.breadcrumb')
    <div class="pad margin no-print">
        <div class="callout callout-info" style="margin-bottom: 0!important;">
            <h4><i class="fa fa-info"></i> Note:</h4>
            Pada halaman ini anda dapat mencetak surat permohonan Kerja Praktek dari mahasiswa dan Menerima atau Menolak permohonan Kerja Praktek dari mahasiswa.
        </div>
    </div>

    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-bookmark"></i> {{$pendaftaran->user->nama}}
                    <small class="pull-right">Tanggal Pendaftaran: {{date('d M Y', strtotime($pendaftaran->pendaftaran->tanggal_input))}}</small>
                </h2>
            </div>
        </div>
        <div class="row invoice-info">
            <div class="col-sm-2 invoice-col">
                Nomor Surat <br>
                Perihal Surat <br>
                Bidang Yang Dipilih <br>
                Asal Kampus <br>
                Program Studi <br>
                Status <br>
                Surat Permohonan<br>
            </div>
            <div class="col-sm-10 invoice-col">
                : {{$pendaftaran->pendaftaran->nomor_surat}} <br>
                : {{$pendaftaran->pendaftaran->perihal}} <br>
                @if ($pendaftaran->pendaftaran->bidang == 1)
                    : Bagian Program, Data Organisasi dan SDM <br>
                @elseif($pendaftaran->pendaftaran->bidang == 2)
                    : Bagian Keuangan, Umum dan Logistik <br>
                @elseif($pendaftaran->pendaftaran->bidang == 3)
                    : Bagian Hukum, Teknis dan Hupmas <br>
                @endif
                : {{$pendaftaran->pendaftaran->asal_kampus}} <br>
                : {{$pendaftaran->pendaftaran->program_studi}} <br>
                @if ($pendaftaran->pendaftaran->status == 0)
                    : <span class="label label-danger">Ditolak</span> <br>
                @elseif($pendaftaran->pendaftaran->status == 2)
                    : <span class="label label-warning">On Proses</span> <br>
                @elseif($pendaftaran->pendaftaran->status == 1)
                    : <span class="label label-success">Disetujui</span> <br>
                @endif
                : Terlampir
            </div>
        </div>

        <hr>
        <div class="row no-print">
            <div class="col-xs-12">
                <a href="{{ route('surat.download',$pendaftaran->pendaftaran->document) }}" class="btn btn-default"><i class="fa fa-print"></i> Print Surat Permohonan</a>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
@endsection
@section('js')
    <script type="text/javascript">
        function editPendaftaran(){
            $('#terimaPendaftaran').modal('show')
        }

        function tolPendaftaran(){
            $('#tolakPendaftaran').modal('show')
        }
    </script>
@endsection