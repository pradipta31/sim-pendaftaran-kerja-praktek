@extends('layouts.master',['activeMenu' => 'pendaftaran'])
@section('title','Data Pendaftaran')
@section('breadcrumb', 'Data Pendaftaran')
@section('detail_breadcrumb', 'Data Pendaftaran Mahasiswa Kerja Praktek')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tabelPendaftaran" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>NIM</th>
                                        <th>Email</th>
                                        <th>Asal Kampus</th>
                                        <th>Status</th>
                                        <th>Nama Petugas</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pendaftarans as $user)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$user->user->nama}}</td>
                                            <td>
                                                {{$user->nim}}
                                            </td>
                                            <td>{{$user->user->email}}</td>
                                            <td>
                                                {{$user->pendaftaran->asal_kampus}}
                                            </td>
                                            <td>
                                                @if ($user->pendaftaran->status == 0)
                                                    <span class="label label-danger">Ditolak</span>
                                                @elseif($user->pendaftaran->status == 2)
                                                    <span class="label label-warning">On Proses</span>
                                                @elseif($user->pendaftaran->status == 1)
                                                    <span class="label label-success">Disetujui</span>
                                                @endif
                                            </td>
                                            <td>
                                                
                                                @if ($user->admin_id == null)
                                                    -
                                                @else
                                                    @php
                                                        $admin = \App\User::where('id',$user->admin_id)->first();
                                                    @endphp
                                                    {{$admin->nama}}
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{url('kabag/pendaftaran/detail/'.$user->id)}}" class="btn btn-sm btn-primary">
                                                    <i class="fa fa-eye"></i>
                                                    Detail
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelPendaftaran').dataTable()
        });

        function editUserModal(){
            $('#editUser').modal('show');
        }
    </script>
@endsection
