@extends('layouts.master',['activeMenu' => 'penilaian'])
@section('title','Data Penilaian')
@section('breadcrumb', 'Data Penilaian')
@section('detail_breadcrumb', 'Data Penilaian Mahasiswa Kerja Praktek')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tabelMahasiswa" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>NIM</th>
                                        <th>Email</th>
                                        <th>Kampus</th>
                                        <th>Tempat</th>
                                        <th>Waktu KP</th>
                                        <th>Penilaian</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($mahasiswas as $user)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$user->user->nama}}</td>
                                            <td>
                                                {{$user->nim}}
                                            </td>
                                            <td>{{$user->user->email}}</td>
                                            <td>
                                                {{$user->pendaftaran->asal_kampus}}
                                            </td>
                                            <td>
                                                @if ($user->pendaftaran->bidang == 1)
                                                    <span class="label label-primary">Bagian Program, Data Organisasi dan SDM</span>
                                                @elseif($user->pendaftaran->bidang == 2)
                                                    <span class="label label-primary">Bagian Keuangan, Umum dan Logistik</span>
                                                @elseif($user->pendaftaran->bidang == 3)
                                                    <span class="label label-primary">Bagian Hukum, Teknis dan Hupmas</span>
                                                @endif
                                            </td>
                                            <td>
                                                {{date('d-m-Y', strtotime($user->tgl_mulai))}} s/d {{date('d-m-Y', strtotime($user->tgl_selesai))}}
                                            </td>
                                            <td>
                                                <a href="{{url('kabag/penilaian/'.$user->user_id.'/mahasiswa')}}" class="btn btn-primary btn-md">
                                                    <i class="fa fa-tasks"></i>
                                                    Lihat Penilaian
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelMahasiswa').dataTable()
        });

        function editUserModal(){
            $('#editUser').modal('show');
        }
    </script>
@endsection
