<header class="main-header">
    <a href="{{url('home')}}" class="logo">
        <span class="logo-mini"><b>SPA</b></span>
        <span class="logo-lg">
        <b>SIM</b> KP</span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @if (Auth::user()->avatar == null)
                            <img src="{{asset('images/ava.png')}}" class="user-image" alt="User Image">
                        @else
                            <img src="{{asset('images/ava/'.Auth::user()->avatar)}}" class="user-image" alt="User Image">
                        @endif
                        <span class="hidden-xs">{{Auth::user()->nama}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            @if (Auth::user()->avatar == null)
                                <img src="{{asset('images/ava.png')}}" class="img-circle" alt="User Image">
                            @else
                                <img src="{{asset('images/ava/'.Auth::user()->avatar)}}" class="img-circle" alt="User Image">
                            @endif
                            <p>
                                {{Auth::user()->nama}}
                                <small>
                                    @if (Auth::user()->level == 1)
                                    Petugas
                                    @elseif (Auth::user()->level == 2)
                                    Kepala Bagian
                                    @elseif (Auth::user()->level == 3)
                                    Mahasiswa
                                    @endif
                                </small>
                            </p>
                        </li>
                        <li class="user-footer">
                            @if (Auth::user()->level == 3)
                                
                            @else
                                <div class="pull-left">
                                    <a href="{{url('petugas/profile')}}" class="btn btn-default btn-flat">Profile</a>
                                </div>
                            @endif
                            <div class="pull-right">
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">
                                    <i class="fa fa-sign-out"></i>
                                    Sign out
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
  