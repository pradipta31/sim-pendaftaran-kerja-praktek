
<li class="header">MASTER NAVIGATION</li>
<li class="{{$activeMenu == 'mahasiswa' ? 'active' : ''}}"><a href="{{url('kabag/mahasiswa')}}"><i class="fa fa-users"></i> <span>Data Mahasiswa</span></a></li>
<li class="{{$activeMenu == 'pendaftaran' ? 'active' : ''}}"><a href="{{url('kabag/pendaftaran')}}"><i class="fa fa-th-list"></i> <span>Data Pendaftaran</span></a></li>
<li class="{{$activeMenu == 'jurnal' ? 'active' : ''}}"><a href="{{url('kabag/jurnal')}}"><i class="fa fa-sticky-note"></i> <span>Data Jurnal & Absensi</span></a></li>
<li class="{{$activeMenu == 'penilaian' ? 'active' : ''}}"><a href="{{url('kabag/penilaian')}}"><i class="fa fa-tasks"></i> <span>Data Penilaian</span></a></li>
