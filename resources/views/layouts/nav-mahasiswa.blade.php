<li class="header">NAVIGASI PENGGUNA</li>
<li class="treeview {{$activeMenu == 'jurnal' ? 'active' : ''}}">
    <a href="#">
        <i class="fa fa-list-alt"></i>
        <span>Jurnal & Absensi</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('mahasiswa/jurnal/tambah')}}"><i class="fa fa-plus"></i> Tambah jurnal Baru</a></li>
        <li><a href="{{url('mahasiswa/jurnal')}}"><i class="fa fa-circle-o"></i> Data Jurnal</a></li>
    </ul>
</li>
<li class="{{$activeMenu == 'profile-mhs' ? 'active' : ''}}"><a href="{{url('mahasiswa/profile-saya')}}"><i class="fa fa-pencil-square-o"></i> <span>Profile Saya</span></a></li>
