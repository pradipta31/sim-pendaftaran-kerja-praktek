<li class="header">NAVIGASI PENGGUNA</li>
<li class="treeview {{$activeMenu == 'pengguna' ? 'active' : ''}}">
    <a href="#">
        <i class="fa fa-users"></i>
        <span>Pengguna</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('petugas/pengguna/tambah')}}"><i class="fa fa-plus"></i> Tambah Pengguna Baru</a></li>
        <li><a href="{{url('petugas/pengguna')}}"><i class="fa fa-circle-o"></i> Data Pengguna</a></li>
    </ul>
</li>
<li class="{{$activeMenu == 'mahasiswa' ? 'active' : ''}}"><a href="{{url('petugas/mahasiswa')}}"><i class="fa fa-users"></i> <span>Data Mahasiswa</span></a></li>
<li class="{{$activeMenu == 'pendaftaran' ? 'active' : ''}}"><a href="{{url('petugas/pendaftaran')}}"><i class="fa fa-th-list"></i> <span>Data Pendaftaran</span></a></li>

<li class="header">MASTER NAVIGATION</li>
<li class="{{$activeMenu == 'jurnal' ? 'active' : ''}}"><a href="{{url('petugas/jurnal')}}"><i class="fa fa-sticky-note"></i> <span>Data Jurnal & Absensi</span></a></li>
<li class="{{$activeMenu == 'penilaian' ? 'active' : ''}}"><a href="{{url('petugas/penilaian')}}"><i class="fa fa-tasks"></i> <span>Data Penilaian</span></a></li>

<li class="header">NAVIGASI NOTIFIKASI</li>
<li class="treeview {{$activeMenu == 'pengumuman' ? 'active' : ''}}">
    <a href="#">
        <i class="fa fa-bell"></i>
        <span>Data Pengumuman</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('petugas/pengumuman/tambah')}}"><i class="fa fa-plus"></i> Tambah Pengumuman</a></li>
        <li><a href="{{url('petugas/pengumuman')}}"><i class="fa fa-circle-o"></i> Data Pengumuman</a></li>
    </ul>
</li>
