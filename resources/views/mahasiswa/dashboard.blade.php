@extends('layouts.master',['activeMenu' => 'dashboard'])
@section('title','Dashboard')
@section('breadcrumb', 'Dashboard')
@section('detail_breadcrumb', 'Control Panel')
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-md-12">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Selamat datang {{Auth::user()->nama}}</h4>
                    Pada halaman dashboard anda dapat melihat beberapa informasi mengenai website anda.
                </div>

                <ul class="timeline">
                    <li class="time-label">
                        <span class="bg-red">
                            PENGUMUMAN
                        </span>
                    </li>
                    @foreach ($pengumumans as $pengumuman)
                        <li>
                            <i class="fa fa-bell bg-blue"></i>
                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> {{date('d M, Y H:i:s', strtotime($pengumuman->created_at))}}</span>
                    
                                <h2 class="timeline-header"><a href="#">{{$pengumuman->judul}}</a>{{date('d-m-Y',strtotime($pengumuman->tgl_mulai))}} - {{date('d-m-Y',strtotime($pengumuman->tgl_akhir))}}</h2>
                    
                                <div class="timeline-body">
                                    {!! $pengumuman->isi !!}
                                </div>
                    
                                <div class="timeline-footer">
                                    <h5>
                                        Petugas Kerja Praktek
                                    </h5>
                                </div>
                            </div>
                        </li>
                    @endforeach
                
                </ul>
            </div>
        </div>
    </section>
@endsection

@section('js')
    
@endsection
