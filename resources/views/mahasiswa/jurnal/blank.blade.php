@extends('layouts.master',['activeMenu' => 'jurnal'])
@section('title','Tambah Jurnal')
@section('breadcrumb', 'Tambah Jurnal')
@section('detail_breadcrumb', 'Tambah Jurnal Baru')
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Jurnal sudah di upload!</h3>
            </div>
            <div class="box-body">
                Anda Sudah melakukan absensi hari ini!
            </div>
        </div>
    </section>
@endsection
@section('js')

@endsection