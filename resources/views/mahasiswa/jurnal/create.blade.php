@extends('layouts.master',['activeMenu' => 'jurnal'])
@section('title','Tambah Jurnal')
@section('breadcrumb', 'Tambah Jurnal')
@section('detail_breadcrumb', 'Tambah Jurnal Baru')
@section('css')
    {{-- <link rel="stylesheet" href="{{asset('backend/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}"> --}}
    <link rel="stylesheet" href="{{asset('backend/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <form class="" action="{{url('mahasiswa/jurnal/tambah')}}" name="formUser" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Jurnal Kegiatan</h3>
                            <p>Tambah jurnal kegiatan anda hari ini.</p>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Tanggal Kegiatan</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepiker" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Keterangan</label>
                                <textarea name="keterangan" class="form-control" id="" cols="30" rows="5" placeholder="Masukan detail kegiatan anda">{{old('keterangan')}}</textarea>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
    {{-- <script src="{{asset('backend/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script> --}}
    <script src="{{asset('backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script type="text/javascript">
        $("#datepiker").datepicker().datepicker("setDate", new Date());

    </script>
@endsection