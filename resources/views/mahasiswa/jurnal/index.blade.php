@extends('layouts.master',['activeMenu' => 'jurnal'])
@section('title','Data Jurnal')
@section('breadcrumb', 'Data Jurnal Kegiatan')
@section('detail_breadcrumb', 'Manajemen Data Jurnal Kegiatan')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <a href="{{url('mahasiswa/jurnal/tambah')}}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                            <i class="fa fa-plus"></i>
                            Tambah Jurnal Kegiatan
                        </a>
                        <a href="{{url('mahasiswa/absensi')}}" class="btn btn-success" style="margin-bottom: 10px">
                            <i class="fa fa-check-square-o"></i>
                            Data Absensi
                        </a>
                        <div class="table-responsive">
                            <table id="tabelJurnal" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Petugas</th>
                                        <th>Tanggal Kegiatan</th>
                                        <th>Tanggal Validasi</th>
                                        <th>Keterangan</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($jurnals as $jurnal)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>
                                                @if ($jurnal->admin_id == null)
                                                    -
                                                @else
                                                    {{$jurnal->admin->user->nama}}
                                                @endif
                                            </td>
                                            <td>
                                                {{date('d-m-Y', strtotime($jurnal->tanggal_kegiatan))}}
                                            </td>
                                            <td>
                                                @if ($jurnal->tanggal_validasi == null)
                                                    -
                                                @else
                                                    {{date('d-m-Y', strtotime($jurnal->tanggal_validasi))}}
                                                @endif
                                            </td>
                                            <td>{{$jurnal->keterangan}}</td>
                                            <td>
                                                @if ($jurnal->status == 0)
                                                    <span class="label label-danger">Tidak Di Validasi</span>
                                                @elseif($jurnal->status == 2)
                                                    <span class="label label-warning">Belum di Validasi</span>
                                                @elseif($jurnal->status == 1)
                                                    <span class="label label-success">Sudah Di Validasi</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelJurnal').dataTable()
        });

        function editUserModal(){
            $('#editUser').modal('show');
        }
    </script>
@endsection
