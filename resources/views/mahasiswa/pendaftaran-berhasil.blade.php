<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mahasiswa Kerja Praktek KPU Prov. Bali | Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{asset('backend/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @toastr_css
    <link rel="stylesheet" href="{{asset('backend/bower_components/Ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/plugins/iCheck/square/blue.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
    <style>
        img {
          display: block;
          margin-left: auto;
          margin-right: auto;
        }
    </style>
    <div class="">
        <img src="{{asset('images/icon/info.png')}}" alt="" class="img-responsive" style="width: 20%">
        <div class="text-center">
            <h1>PENDAFTARAN BERHASIL!</h1>
            <div class="row">
                <div class="col-md-4">
                    
                </div>
                <div class="col-md-4">
                    <p style="font-family:'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif">
                        Terimakasih data anda telah kami terima, kami akan mengirimkan informasi mengenai penerimaan pendaftaran melalui Email yang anda gunakan.
                        <br>
                        <a href="{{url('login')}}">Kembali Ke Halaman Utama</a>
                    </p>
                </div>
                <div class="col-md-4">

                </div>
            </div>
        </div>
    </div>

    @jquery
    @toastr_js
    @toastr_render
    <script src="{{asset('backend/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/plugins/iCheck/icheck.min.js')}}"></script>
    <script>
    $(function () {
        $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
        });
    });
    function loginBtn(d){
        d.innerHTML = '<i class="fa fa-spinner fa-spin"></i>';
    }
    </script>
</body>
</html>
