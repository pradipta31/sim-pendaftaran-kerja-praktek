<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mahasiswa Kerja Praktek KPU Prov. Bali | Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{asset('backend/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @toastr_css
    <link rel="stylesheet" href="{{asset('backend/bower_components/Ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/plugins/iCheck/square/blue.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box" style="margin-top: 80px; width: 50%">
    <div class="login-logo">
        <a href="{{url('login')}}"><b>Mahasiswa Kerja Praktek KPU Provinsi Bali</b> Registrasi</a>
    </div>
    <div class="login-box-body">
        <img src="{{asset('images/logo/logo-angkasa-1.png')}}" alt="" class="img-responsive">
        <br>
        <p class="login-box-msg">Mohon untuk mengisi data berikut dengan benar.</p>
        <hr>

        <form action="{{url('registrasi')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <label for="">Nim</label>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" name="nim" value="{{ old('nim')}}" placeholder="Masukan nim anda">
                        <span class="glyphicon glyphicon-menu-left form-control-feedback"></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="">Nama</label>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" name="nama" value="{{ old('nama')}}" placeholder="Masukan nama anda">
                        <span class="glyphicon glyphicon-menu-left form-control-feedback"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label for="">Email</label>
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" name="email" value="{{ old('email')}}" placeholder="Masukan email">
                        <span class="glyphicon glyphicon-menu-left form-control-feedback"></span>
                        <small>NB: Email yang digunakan harus aktif.</small>
                    </div>
                    
                </div>
                <div class="col-md-6">
                    <label for="">Nomor HP</label>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" name="no_telp" value="{{ old('no_telp')}}" placeholder="Masukan Nomor HP">
                        <span class="glyphicon glyphicon-menu-left form-control-feedback"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label for="">Nomor Surat</label>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" name="nomor_surat" value="{{ old('nomor_surat')}}" placeholder="Masukan nomor surat">
                        <span class="glyphicon glyphicon-menu-left form-control-feedback"></span>
                        <small>Nb: Nomor surat yang didapat dari kampus.</small>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="">Perihal</label>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" name="perihal" value="{{ old('perihal')}}" placeholder="Masukan perihal surat">
                        <span class="glyphicon glyphicon-menu-left form-control-feedback"></span>
                        <small>Masukan perihal surat yang didapat dari kampus</small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label for="">Asal Kampus</label>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" name="asal_kampus" value="{{ old('asal_kampus')}}" placeholder="Masukan asal Kampus">
                        <span class="glyphicon glyphicon-menu-left form-control-feedback"></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="">Program Studi</label>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" name="program_studi" value="{{ old('program_studi')}}" placeholder="Masukan Program Studi">
                        <span class="glyphicon glyphicon-menu-left form-control-feedback"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label for="">Jenjang Studi</label>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" name="jenjang_studi" value="{{ old('jenjang_studi')}}" placeholder="Masukan Jenjang Studi">
                        <span class="glyphicon glyphicon-menu-left form-control-feedback"></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="">Bidang Yang Diminati</label>
                    <div class="form-group has-feedback">
                        <select name="bidang" class="form-control">
                            <option value="">Pilih Bidang</option>
                            <option value="1">Bagian Program, Data Organisasi dan SDM</option>
                            <option value="2">Bagian Keuangan, Umum dan Logistik</option>
                            <option value="3">Bagian Hukum, Teknis dan Hupmas</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">Scan Surat Permohonan Kerja Praktek</label>
                    <div class="form-group">
                        <input type="file" name="document" class="form-control">
                        <small>NB: File harus berbentuk .pdf</small>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-8">
                    <a href="{{url('login')}}" class="btn btn-default">Halaman Login</a>
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block" onclick="loginBtn(this);">
                    Register
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@jquery
@toastr_js
@toastr_render
<script src="{{asset('backend/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('backend/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('backend/plugins/iCheck/icheck.min.js')}}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%'
    });
  });
  function loginBtn(d){
    d.innerHTML = '<i class="fa fa-spinner fa-spin"></i>';
  }
</script>
</body>
</html>
