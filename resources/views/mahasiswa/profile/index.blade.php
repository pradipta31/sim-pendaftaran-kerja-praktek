@extends('layouts.master',['activeMenu' => 'profile-mhs'])
@section('title', 'Profile Mahasiswa')
@section('breadcrumb', 'Profile Mahasiswa')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <div class="text-center">
                            @if ($mahasiswa->user->avatar == null)
                                <img class="profile-user-img img-fluid img-circle"
                                    src="{{asset('images/ava.png')}}"
                                    alt="User profile picture">
                            @else
                                <img class="profile-user-img img-fluid img-circle"
                                    src="{{asset('images/ava/'.$mahasiswa->user->avatar)}}"
                                    alt="User profile picture">
                            @endif
                        </div>
    
                        <h3 class="profile-username text-center">{{$mahasiswa->user->nama}}</h3>
                        <?php
                            $mulai = date_create($mahasiswa->tgl_mulai);
                            $selesai = date_create($mahasiswa->tgl_selesai);
                            $diff  = date_diff( $mulai, $selesai );
                        ?>
                        <p class="text-muted text-center">Mahasiswa</p>
                        <p class="text-center">{{$mahasiswa->pendaftaran->asal_kampus}}</p>
    
                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Masa KP</b> <span class="pull-right">{{date('d-m-Y', strtotime($mahasiswa->tgl_mulai))}} s/d {{date('d-m-Y', strtotime($mahasiswa->tgl_selesai))}} ({{$diff->days}} Hari)</span>
                            </li>
                            <li class="list-group-item">
                                <b>Penempatan</b> <a class="pull-right" style="">
                                    @if ($mahasiswa->pendaftaran->bidang == 1)
                                        Bagian Program, Data Organisasi dan SDM <br>
                                    @elseif($mahasiswa->pendaftaran->bidang == 2)
                                        Bagian Keuangan, Umum dan Logistik <br>
                                    @elseif($mahasiswa->pendaftaran->bidang == 3)
                                        Bagian Hukum, Teknis dan Hupmas <br>
                                    @endif
                                </a>
                            </li>
                            <li class="list-group-item">
                                <b>Nama</b> <a class="pull-right">{{$mahasiswa->user->nama}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Email</b> <a class="pull-right">{{$mahasiswa->user->email}}</a>
                            </li>
                        </ul>
    
                        <button class="btn btn-primary btn-block" id="editProfile">
                            <i class="fa fa-pencil"></i>
                            Edit Profile
                        </button>
                        <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#editAva{{$mahasiswa->id}}">
                            <i class="fa fa-pencil-square-o"></i>
                            Edit Foto
                        </button>

                        <div class="modal fade" id="editAva{{$mahasiswa->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 class="modal-title" id="exampleModalLabel">Ubah Foto Anda</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="{{url('mahasiswa/avatar/'.$mahasiswa->id.'/edit')}}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="put">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Masukan Foto Anda</label>
                                                        <input type="file" class="form-control" name="avatar">
                                                        <small>NB: Foto yang diunggah harus berpakaian rapi dengan ukuran maks 2MB.</small>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for=""></label>                                                        
                                                        @if ($mahasiswa->user->avatar == null)
                                                            <span>Foto tidak ditemukan.</span>
                                                        @else
                                                            <a href="{{asset('images/ava/'.$mahasiswa->user->avatar)}}" target="_blank">
                                                                <img class="profile-user-img img-fluid"
                                                                    src="{{asset('images/ava/'.$mahasiswa->user->avatar)}}"
                                                                    alt="User profile picture">
                                                            </a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                            <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan & Kirim</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="box box-primary" id="formProfile" style="display: none">
                    <div class="box-body">
                        <form action="{{url('mahasiswa/profile-saya/'.$mahasiswa->id.'/edit')}}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="_method" value="put">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">NIM</label>
                                <div class="col-sm-10">
                                    <input type="text" name="nim" class="form-control" placeholder="Masukan NIM (Nomor Induk Mahasiswa)" value="{{$mahasiswa->nim}}" disabled>
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10">
                                    <input type="text" name="nama" class="form-control" placeholder="Masukan nama" value="{{$mahasiswa->user->nama}}">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" name="email" class="form-control" placeholder="Masukan email" value="{{$mahasiswa->user->email}}" readonly>
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" name="password" id="password" class="form-control" placeholder="Masukan password">
                                    <small>Kosongkan jika tidak ingin mengubah password</small>
                                </div>
                                
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Re-type Password</label>
                                <div class="col-sm-10">
                                    <input type="password" name="confirmation_password" id="confirmation_password" class="form-control" placeholder="Masukan password ulang">
                                    <span id="message"></span>
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Alamat</label>
                                <div class="col-sm-10">
                                    <textarea name="alamat" class="form-control" cols="30" rows="3">{{$mahasiswa->alamat}}</textarea>
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">No Handphone</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" placeholder="Masukan No HP" name="no_telp" value="{{$mahasiswa->no_telp}}">
                                </div>
                            </div>
    
                            
                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-save"></i>
                                        Simpan Perubahan
                                    </button>
                                </div>
                            </div>
                        </form>
                            
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
        function editAvaModal(){
            $('#editAva').modal('show')
        }
        $(document).ready(function(){
            $('#editProfile').click( function() {
                $('#formProfile').toggle('slow');
            });
        });

        $('#password, #confirmation_password').on('keyup', function () {
            if ($('#password').val() == $('#confirmation_password').val()) {
                $('#message').html('Password dapat digunakan!').css('color', 'green');
            } else {
                $('#message').html('Password tidak sama!').css('color', 'red');
            }
        });
    </script>
@endsection