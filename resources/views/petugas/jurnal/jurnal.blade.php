@extends('layouts.master',['activeMenu' => 'jurnal'])
@section('title','Data Jurnal')
@section('breadcrumb', 'Data Jurnal Kegiatan')
@section('detail_breadcrumb', 'Manajemen Data Jurnal Kegiatan')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <a href="{{url('petugas/jurnal/cetak/'.$mhs->id)}}" class="btn btn-primary" style="margin-bottom: 10px">
                            <i class="fa fa-print"></i>
                            Cetak Jurnal
                        </a>
                        <div class="table-responsive">
                            <table id="tabelJurnal" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Petugas</th>
                                        <th>Tanggal Kegiatan</th>
                                        <th>Tanggal Validasi</th>
                                        <th>Keterangan</th>
                                        <th>Status</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($jurnals as $jurnal)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>
                                                @if ($jurnal->admin_id == null)
                                                    -
                                                @else
                                                    {{$jurnal->admin->user->nama}}
                                                @endif
                                            </td>
                                            <td>
                                                {{date('d-m-Y', strtotime($jurnal->tanggal_kegiatan))}}
                                            </td>
                                            <td>
                                                @if ($jurnal->tanggal_validasi == null)
                                                    -
                                                @else
                                                    {{date('d-m-Y', strtotime($jurnal->tanggal_validasi))}}
                                                @endif
                                            </td>
                                            <td>{{$jurnal->keterangan}}</td>
                                            <td>
                                                @if ($jurnal->status == 0)
                                                    <span class="label label-danger">Tidak Di Setujui</span>
                                                @elseif($jurnal->status == 2)
                                                    <span class="label label-warning">Belum di Setujui</span>
                                                @elseif($jurnal->status == 1)
                                                    <span class="label label-success">Sudah Di Setujui</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($jurnal->tanggal_validasi == null)
                                                    <a href="javascript:void(0);" class="btn btn-primary" onclick="validasiJurnal('{{$jurnal->id}}')">
                                                        <i class="fa fa-check"></i>
                                                        Setujui
                                                    </a>
                                                    <a href="javascript:void(0);" class="btn btn-danger" onclick="tolakValidasi('{{$jurnal->id}}')">
                                                        <i class="fa fa-ban"></i>
                                                        Tolak
                                                    </a>
                                                @else
                                                    @if ($jurnal->status == 0)
                                                        <span class="label label-danger">Tidak Di Setujui</span>
                                                    @elseif($jurnal->status == 2)
                                                        <span class="label label-warning">Belum di Setujui</span>
                                                    @elseif($jurnal->status == 1)
                                                        <span class="label label-success">Sudah Di Setujui</span>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form class="hidden" action="" method="post" id="formSetuju">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
    </form>
    <form class="hidden" action="" method="post" id="formTolak">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
    </form>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelJurnal').dataTable()
        });

        function editUserModal(){
            $('#editUser').modal('show');
        }

        function validasiJurnal(id){
            swal({
                title: "Anda yakin?",
                text: "Jurnal dari mahasiswa yang bersangkutan akan di validasi dan akan dilakukan absensi hadir.",
                icon: "warning",
                buttons: true,
                dangerMode: false
            })
            .then((willActive) => {
                if (willActive) {
                    swal("Berhasil! Jurnal mahasiswa berhasil di validasi dan mahasiswa berhasil di absen!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formSetuju').attr('action', '{{url('petugas/jurnal/setuju')}}/'+id);
                        $('#formSetuju').submit();
                    }); 
                }
            });
        }

        function tolakValidasi(id){
            swal({
                title: "Anda yakin?",
                text: "Jurnal dari mahasiswa yang bersangkutan akan tidak di validasi dan mahasiswa akan diabsen tidak hadir!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willActive) => {
                if (willActive) {
                    swal("Jurnal mahasiswa telah tidak di validasi dan mahasiswa dianggap tidak hadir!", {
                        icon: "error",
                    }).then((res) => {
                        $('#formTolak').attr('action', '{{url('petugas/jurnal/tolak')}}/'+id);
                        $('#formTolak').submit();
                    }); 
                }
            });
        }
    </script>
@endsection
