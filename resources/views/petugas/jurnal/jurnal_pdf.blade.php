<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Jurnal Mahasiswa</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
    <style>
        /* table, td, th {
            border: 1px solid black;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        } */
        .tb{
            border: 1px solid black;
        }
    </style>
	<center>
		<h5 style="text-decoration: underline">JURNAL MAHASISWA 
            <br>
            KERJA PRAKTEK (KP)</h4>
    </center>
    <br>
    <small>
        <table>
            <tbody>
                <tr>
                    <td>NIM</td><td> : </td><td>{{$mhs->mahasiswa->nim}}</td>
                </tr>
                <tr>
                    <td>Nama Mahasiswa</td><td> : </td><td>{{$mhs->mahasiswa->user->nama}}</td>
                </tr>
                <tr>
                    <td>Program Studi</td><td> : </td><td>{{$mhs->mahasiswa->pendaftaran->program_studi}}</td>
                </tr>
            </tbody>
        </table>
    </small>

    <small style="font-size: 10px">Detail kegiatan mahasiswa kerja praktek di instansi adalah sebagai berikut:</small>

	<small>
        <table style="width: 100%; border-collapse: collapse; border: 1px solid black; text-align: center">
            <thead>
                <tr>
                    <th style="border: 1px solid black"><center>No</center></th>
                    <th style="border: 1px solid black"><center>Tanggal Kegiatan</center></th>
                    <th style="border: 1px solid black"><center>Tanggal Validasi</center></th>
                    <th style="border: 1px solid black"><center>Keterangan</center></th>
                    <th style="border: 1px solid black"><center>Petugas</center></th>
                    <th style="border: 1px solid black"><center>Status</center></th>
                </tr>
            </thead>
            <tbody>
                @php
                    $no = 1;
                @endphp
                @foreach ($jurnal as $j)
                <tr>
                    <td style="border: 1px solid black">{{$no++}}</td>
                    <td style="border: 1px solid black">{{date('d-m-Y', strtotime($j->tanggal_kegiatan))}}</td>
                    <td style="border: 1px solid black">
                        @if ($j->tanggal_validasi == null)
                            -
                        @else
                            {{date('d-m-Y', strtotime($j->tanggal_validasi))}}
                        @endif
                    </td>
                    <td style="border: 1px solid black">{{$j->keterangan}}</td>
                    <td style="border: 1px solid black">{{$j->admin->user->nama}}</td>
                    <td style="border: 1px solid black">
                        @if ($j->status == 0)
                            Dalam Proses
                        @elseif ($j->status == 1)
                            Disetujui
                        @elseif ($j->status == 2)
                            Ditolak
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </small>

</body>
</html>