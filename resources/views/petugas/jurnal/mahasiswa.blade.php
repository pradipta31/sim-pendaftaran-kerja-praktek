@extends('layouts.master',['activeMenu' => 'jurnal'])
@section('title','Data Mahasiswa')
@section('breadcrumb', 'Data Mahasiswa')
@section('detail_breadcrumb', 'Data Mahasiswa Kerja Praktek')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tabelMahasiswa" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>NIM</th>
                                        <th>Email</th>
                                        <th>Tempat</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($mahasiswas as $user)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$user->user->nama}}</td>
                                            <td>
                                                {{$user->nim}}
                                            </td>
                                            <td>{{$user->user->email}}</td>
                                            <td>
                                                @if ($user->pendaftaran->bidang == 1)
                                                    Bagian Program, Data Organisasi dan SDM
                                                @elseif($user->pendaftaran->bidang == 2)
                                                    Bagian Keuangan, Umum dan Logistik
                                                @elseif($user->pendaftaran->bidang == 3)
                                                    Bagian Hukum, Teknis dan Hupmas
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{url('petugas/mahasiswa/'.$user->id.'/jurnal')}}" class="btn btn-primary">
                                                    <i class="fa fa-list-alt"></i>
                                                    Jurnal
                                                </a>
                                                <a href="{{url('petugas/mahasiswa/'.$user->id.'/absensi')}}" class="btn btn-success">
                                                    <i class="fa fa-check-square-o"></i>
                                                    Absensi
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelMahasiswa').dataTable()
        });
    </script>
@endsection
