@extends('layouts.master',['activeMenu' => 'mahasiswa'])
@section('title','Edit Mahasiswa')
@section('breadcrumb', 'Edit Mahasiswa')
@section('detail_breadcrumb', 'Edit Mahasiswa '.$mahasiswa->nama)
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <form class="" action="{{url('petugas/mahasiswa/'.$mahasiswa->user_id.'/edit')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Mahasiswa</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-md-12">
                                <label for="">NIM Mahasiswa</label>
                                <input type="text" name="nim" class="form-control" placeholder="Masukan Nama" value="{{$mahasiswa->nim}}" readonly>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Email Mahasiswa</label>
                                <input type="email" name="email" class="form-control" placeholder="Masukan Nama" value="{{$mahasiswa->email}}" readonly>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Nama Mahasiswa</label>
                                <input type="text" name="nama" class="form-control" placeholder="Masukan Nama" value="{{$mahasiswa->nama}}">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Alamat Mahasiswa</label>
                                <textarea name="alamat" id="" cols="30" rows="3" class="form-control">{{$mahasiswa->alamat}}</textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">No Telepon Mahasiswa</label>
                                <input type="text" name="no_telp" class="form-control" placeholder="Masukan Nomor Telepon" value="{{$mahasiswa->no_telp}}">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Status</label>
                                <select name="status" id="" class="form-control" value="{{$mahasiswa->status}}">
                                    <option value="">Pilih Status</option>
                                    <option value="1" {{$mahasiswa->status == 1 ? 'selected' : ''}}>Aktif</option>
                                    <option value="0" {{$mahasiswa->status == 0 ? 'selected' : ''}}>Non Aktif</option>
                                </select>
                            </div>
                            
                            <div class="box-footer col-md-12">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                                <a href="javascript:void(0);" onclick="updatePassword('{{$mahasiswa->user_id}}')" class="btn btn-default">Ganti Password</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
    <form class="hidden" action="" method="post" id="formPassword">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="password" value="" id="password">
    </form>
@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script type="text/javascript">
        function updatePassword(id_user){
            bootbox.prompt({
                title: 'Masukan password',
                inputType: 'password',
                size: 'small',
                callback: function(result){
                    if (result != null ) {
                        bootbox.prompt({
                        title: 'Masukan password kembali.',
                        inputType: 'password',
                        size: 'small',
                            callback: function(password){
                                if (password != null) {
                                    if (result == password) {
                                        $('#password').val(password);
                                        $('#formPassword').attr('action', '{{url('petugas/mahasiswa/chpass')}}/'+id_user);
                                        $('#formPassword').submit();
                                    }else {
                                        bootbox.alert("Password tidak sama. Silakan ulangi!");
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }
    </script>
@endsection