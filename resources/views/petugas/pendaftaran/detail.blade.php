@extends('layouts.master',['activeMenu' => 'pendaftaran'])
@section('title','Data Pendaftaran')
@section('breadcrumb', 'Data Pendaftaran')
@section('detail_breadcrumb', 'Data Pendaftaran Mahasiswa Kerja Praktek')
@section('content')
    @include('layouts.breadcrumb')
    <div class="pad margin no-print">
        <div class="callout callout-info" style="margin-bottom: 0!important;">
            <h4><i class="fa fa-info"></i> Note:</h4>
            Pada halaman ini anda dapat mencetak surat permohonan Kerja Praktek dari mahasiswa dan Menerima atau Menolak permohonan Kerja Praktek dari mahasiswa.
        </div>
    </div>

    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-bookmark"></i> {{$pendaftaran->user->nama}}
                    <small class="pull-right">Tanggal Pendaftaran: {{date('d M Y', strtotime($pendaftaran->pendaftaran->tanggal_input))}}</small>
                </h2>
            </div>
        </div>
        <div class="row invoice-info">
            <div class="col-sm-2 invoice-col">
                Nomor Surat <br>
                Perihal Surat <br>
                Bidang Yang Dipilih <br>
                Asal Kampus <br>
                Program Studi <br>
                Status <br>
                Surat Permohonan<br>
            </div>
            <div class="col-sm-10 invoice-col">
                : {{$pendaftaran->pendaftaran->nomor_surat}} <br>
                : {{$pendaftaran->pendaftaran->perihal}} <br>
                @if ($pendaftaran->pendaftaran->bidang == 1)
                    : Bagian Program, Data Organisasi dan SDM <br>
                @elseif($pendaftaran->pendaftaran->bidang == 2)
                    : Bagian Keuangan, Umum dan Logistik <br>
                @elseif($pendaftaran->pendaftaran->bidang == 3)
                    : Bagian Hukum, Teknis dan Hupmas <br>
                @endif
                : {{$pendaftaran->pendaftaran->asal_kampus}} <br>
                : {{$pendaftaran->pendaftaran->program_studi}} <br>
                @if ($pendaftaran->pendaftaran->status == 0)
                    : <span class="label label-danger">Ditolak</span> <br>
                @elseif($pendaftaran->pendaftaran->status == 2)
                    : <span class="label label-warning">On Proses</span> <br>
                @elseif($pendaftaran->pendaftaran->status == 1)
                    : <span class="label label-success">Disetujui</span> <br>
                @endif
                : Terlampir
            </div>
        </div>

        <hr>
        <div class="row no-print">
            <div class="col-xs-12">
                <a href="{{ route('surat.download',$pendaftaran->pendaftaran->document) }}" class="btn btn-default"><i class="fa fa-print"></i> Print Surat Permohonan</a>
                @if ($pendaftaran->pendaftaran->status == 2)
                    <a href="javascript:void(0);" class="btn btn-primary pull-right" data-toggle="modal" data-target="#terimaPendaftaran{{$pendaftaran->id}}">
                        <i class="fa fa-check"></i>
                        Terima
                    </a>

                    <a href="javascript:void(0);" class="btn btn-danger pull-right" style="margin-right: 5px" data-toggle="modal" data-target="#tolakPendaftaran{{$pendaftaran->id}}">
                        <i class="fa fa-ban"></i>
                        Tolak
                    </a>

                    <!-- MODAL ACCEPT -->
                    <div class="modal fade" id="terimaPendaftaran{{$pendaftaran->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title" id="exampleModalLabel">Mahasiswa: {{$pendaftaran->user->nama}}</h3>
                                    <p>Pendaftaran mahasiswa Kerja Praktek akan diterima dan mahasiswa akan menerima email penerimaan.</p>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{url('petugas/pendaftaran/setuju/'.$pendaftaran->id)}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="put">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">Tanggal Mulai</label>
                                                    <input type="date" class="form-control" name="tgl_mulai" value="{{old('tgl_mulai')}}">
                                                    <small>Masukan tanggal mulai kerja praktek.</small>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">Tanggal Selesai</label>
                                                    <input type="date" class="form-control" name="tgl_selesai" value="{{old('tgl_selesai')}}">
                                                    <small>Masukan tanggal selesai kerja praktek.</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                        <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan & Kirim</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- MODAL DECLINE -->
                    <div class="modal fade" id="tolakPendaftaran{{$pendaftaran->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title" id="exampleModalLabel">Mahasiswa: {{$pendaftaran->user->nama}}</h3>
                                    <p>Pendaftaran mahasiswa Kerja Praktek akan ditolak dan mahasiswa akan menerima email penolakan.</p>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{url('petugas/pendaftaran/tolak/'.$pendaftaran->id)}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="put">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">Keterangan Penolakan</label>
                                                    <textarea name="keterangan" class="form-control" rows="5" cols="30">{{old('keterangan')}}</textarea>
                                                    <small>Masukan keterangan penolakan terhadap mahasiswa.</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                        <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan & Kirim</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @else
                    <a href="javascript:void(0);" class="btn btn-primary pull-right" disabled>
                        <i class="fa fa-check"></i>
                        Terima
                    </a>

                    <a href="javascript:void(0);" class="btn btn-danger pull-right" style="margin-right: 5px" disabled>
                        <i class="fa fa-ban"></i>
                        Tolak
                    </a>

                    
                @endif
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
@endsection
@section('js')
    <script type="text/javascript">
        function editPendaftaran(){
            $('#terimaPendaftaran').modal('show')
        }

        function tolPendaftaran(){
            $('#tolakPendaftaran').modal('show')
        }
    </script>
@endsection