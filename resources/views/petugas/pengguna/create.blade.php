@extends('layouts.master',['activeMenu' => 'pengguna'])
@section('title','Tambah Pengguna')
@section('breadcrumb', 'Tambah Pengguna')
@section('detail_breadcrumb', 'Tambah Pengguna Baru')
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <form class="" action="{{url('petugas/pengguna/tambah')}}" name="formUser" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="length" value="6">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Pengguna Baru</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">NIP</label>
                                <input type="text" name="nip" class="form-control" placeholder="Masukan NIP" value="{{old('nip')}}">
                            </div>
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input type="text" name="nama" class="form-control" value="{{old('nama')}}" placeholder="Masukan Nama">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" name="email" class="form-control" value="{{old('email')}}" placeholder="Masukkan Email">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-9">
                                        <label class="col-form-label">Password</label>
                                        <input type="text" class="form-control" name="password" value="{{old('password')}}">
                                    </div>
                                    <div class="col-md-3">
                                        <label class="col-form-label"></label>
                                        <button type="button" class="btn btn-success btn-md" onclick="generate();" style="margin-top: 20%">
                                            <i class="fa fa-refresh"></i>
                                            Generate
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Level</label>
                                <select name="level" class="form-control" value="{{old('level')}}">
                                    <option value="">Pilih Level</option>
                                    <option value="1">Petugas</option>
                                    @if ($kabag < 1)
                                        <option value="2">Kepala Bagian</option>
                                    @else

                                    @endif
                                </select>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
        function randomPassword(length) {
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        }

        function generate() {
            formUser.password.value = randomPassword(formUser.length.value);
        }
    </script>
@endsection