@extends('layouts.master',['activeMenu' => 'pengguna'])
@section('title','Edit Pengguna')
@section('breadcrumb', 'Edit Pengguna')
@section('detail_breadcrumb', 'Edit Pengguna Baru')
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <form class="" action="{{url('petugas/pengguna/'.$user->user_id.'/edit')}}" name="formUser" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
            <input type="hidden" name="length" value="6">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Pengguna</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">NIP</label>
                                <input type="text" name="nip" class="form-control" placeholder="Masukan NIP" value="{{$user->nip}}">
                            </div>
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input type="text" name="nama" class="form-control" value="{{$user->user->nama}}" placeholder="Masukan Nama">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" name="email" class="form-control" value="{{$user->user->email}}" placeholder="Masukkan Email">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-9">
                                        <label class="col-form-label">Password</label>
                                        <input type="text" class="form-control" name="password" value="{{old('password')}}">
                                    </div>
                                    <div class="col-md-3">
                                        <label class="col-form-label"></label>
                                        <button type="button" class="btn btn-success btn-md" onclick="generate();" style="margin-top: 20%">
                                            <i class="fa fa-refresh"></i>
                                            Generate
                                        </button>
                                    </div>
                                </div>
                                <small>NB: Kosongkan jika tidak ingin mengubah password</small>
                            </div>
                            <div class="form-group">
                                <label for="">Level</label>
                                <select name="level" class="form-control" value="{{$user->user->level}}" disabled>
                                    <option value="">Pilih Level</option>
                                    <option value="1" {{$user->user->level == '1' ? 'selected' : ''}}>Petugas</option>
                                    <option value="2" {{$user->user->level == '2' ? 'selected' : ''}}>Kepala Bagian</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Status</label>
                                <select name="status" class="form-control" value="{{$user->user->status}}">
                                    <option value="">Pilih Status</option>
                                    <option value="1" {{$user->user->status == 1 ? 'selected' : ''}}>Aktif</option>
                                    <option value="2" {{$user->user->status == 2 ? 'selected' : ''}}>Non Aktif</option>
                                </select>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
        function randomPassword(length) {
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        }

        function generate() {
            formUser.password.value = randomPassword(formUser.length.value);
        }
    </script>
@endsection