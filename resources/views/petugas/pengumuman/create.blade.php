@extends('layouts.master',['activeMenu' => 'pengumuman'])
@section('title','Tambah Pengumuman')
@section('breadcrumb', 'Tambah Pengumuman')
@section('detail_breadcrumb', 'Tambah Pengumuman Baru')
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <form class="" action="{{url('petugas/pengumuman/tambah')}}" name="formUser" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="length" value="6">
            <div class="row">
                <div class="col-md-8">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Pengumuman Baru</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-md-12">
                                <label for="">Judul Pengumuman</label>
                                <input type="text" name="judul" class="form-control" placeholder="Masukan Judul" value="{{old('judul')}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Tanggal Mulai Pengumuman</label>
                                <input type="date" name="tgl_mulai" class="form-control" value="{{old('tgl_mulai')}}" style="height: 25%">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Tanggal Akhir Pengumuman</label>
                                <input type="date" name="tgl_akhir" class="form-control" value="{{old('tgl_akhir')}}" style="height: 25%">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Isi Pengumuman</label>
                                <textarea name="isi" class="form-control" cols="30" rows="10">{{old('isi')}}</textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Status</label>
                                <select name="status" class="form-control" value="{{old('status')}}">
                                    <option value="">Pilih Status</option>
                                    <option value="1">Aktif</option>
                                    <option value="2">Non Aktif</option>
                                </select>
                                <small>NB: Status Aktif untuk menampilkan pengumuman kepada mahasiswa</small>
                            </div>
                            
                            <div class="box-footer col-md-12">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">
                                    <i class="fa fa-save"></i>
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
<script src="{{asset('backend/plugins/tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('backend/plugins/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript">
    tinymce.init({
        selector: 'textarea',
        resize: false,
        plugins: [
            ' advlist anchor autolink codesample fullscreen help',
            ' lists link media noneditable preview',
            ' searchreplace table visualblocks wordcount'
        ],

        toolbar:
        'insertfile undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist',
    });
</script>
@endsection