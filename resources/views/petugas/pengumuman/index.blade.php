@extends('layouts.master',['activeMenu' => 'pengumuman'])
@section('title','Manajemen Pengumuman')
@section('breadcrumb', 'Data Pengumuman')
@section('detail_breadcrumb', 'Manajemen Data Pengumuman')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        {{-- <a href="{{url('petugas/pengumuman/tambah')}}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                            <i class="fa fa-plus"></i>
                            Tambah Pengumuman Baru
                        </a> --}}
                        <div class="table-responsive">
                            <table id="tabelPengumuman" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Admin</th>
                                        <th>Judul</th>
                                        <th>Isi</th>
                                        <th>Status</th>
                                        <th>Created At</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pengumumans as $p)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$p->admin->user->nama}}</td>
                                            <td>{{$p->judul}}</td>
                                            <td>
                                                {!! $p->isi !!}
                                            </td>
                                            <td>
                                                @if ($p->status == 1)
                                                    <span class="label label-primary">Aktif</span>
                                                @else
                                                    <span class="label label-warning">Non Aktif</span>
                                                @endif
                                            </td>
                                            <td>{{$p->created_at}}</td>
                                            <td>
                                                <a href="{{url('petugas/pengumuman/'.$p->id.'/edit')}}" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-pencil"></i>
                                                    Edit
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelPengumuman').dataTable()
        });

        function editUserModal(){
            $('#editUser').modal('show');
        }
    </script>
@endsection
