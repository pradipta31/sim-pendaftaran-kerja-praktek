@extends('layouts.master',['activeMenu' => 'penilaian'])
@section('title','Edit Penilaian')
@section('breadcrumb', 'Edit Penilaian')
@section('detail_breadcrumb', 'Edit Penilaian Kerja Praktek')
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <form class="" action="{{url('petugas/penilaian/'.$penilaian->id.'/edit')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
            <input type="hidden" name="mahasiswa_id" value="{{$penilaian->mahasiswa_id}}">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Penilaian</h3>
                            <p>
                                Mahasiswa: {{$penilaian->mahasiswa->user->nama}}
                            </p>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Kemampuan Beradaptasi</label>
                                <input type="number" name="adaptasi" class="form-control nilai" min="0" max="100" value="{{$penilaian->adaptasi}}" placeholder="Masukan nilai Kemampuan Beradaptasi">
                                <small>Nilai: 0-100</small>
                            </div>
                            <div class="form-group">
                                <label for="">Kemampuan Kerjasama Dalam kelompok</label>
                                <input type="number" name="kerjasama" class="form-control nilai" min="0" max="100" value="{{$penilaian->kerjasama}}" placeholder="Masukan nilai Kemampuan Kerjasama Dalam kelompok">
                                <small>Nilai: 0-100</small>
                            </div>
                            <div class="form-group">
                                <label for="">Kesungguhan Dalam Bekerja</label>
                                <input type="number" name="kesungguhan" class="form-control nilai" min="0" max="100" value="{{$penilaian->kesungguhan}}" placeholder="Masukan nilai Kesungguhan Dalam Bekerja</label>">
                                <small>Nilai: 0-100</small>
                            </div>
                            <div class="form-group">
                                <label for="">Kehadiran</label>
                                <input type="number" name="kehadiran" class="form-control nilai" min="0" max="100" value="{{$penilaian->kehadiran}}" placeholder="Masukan nilai Kehadiran">
                                <small>Nilai: 0-100</small>
                            </div>
                            <div class="form-group">
                                <label for="">Tanggung Jawab Terhadap Pekerjaan</label>
                                <input type="number" name="tanggung_jawab" class="form-control nilai" min="0" max="100" value="{{$penilaian->tanggung_jawab}}" placeholder="Masukan nilai Tanggung Jawab Terhadap Pekerjaan">
                                <small>Nilai: 0-100</small>
                            </div>
                            <div class="form-group">
                                <label for="">Pemahaman Tugas</label>
                                <input type="number" name="pemahaman_tugas" class="form-control nilai" min="0" max="100" value="{{$penilaian->pemahaman_tugas}}" placeholder="Masukan nilai Pemahaman Tugas">
                                <small>Nilai: 0-100</small>
                            </div>
                            <div class="form-group">
                                <label for="">Inisiatif</label>
                                <input type="number" name="inisiatif" class="form-control nilai" min="0" max="100" value="{{$penilaian->inisiatif}}" placeholder="Masukan nilai Inisiatif">
                                <small>Nilai: 0-100</small>
                            </div>

                            <div class="form-group">
                                <label for="">Total</label>
                                <input type="number" name="total" class="form-control" id="result" value="{{$penilaian->total_nilai}}" placeholder="Total Nilai" readonly>
                            </div>

                            <div class="form-group">
                                <label for="">Nilai Rata-rata</label>
                                <input type="number" name="rata" class="form-control" id="rata" value="{{$penilaian->rata_rata}}" min="0" max="100" placeholder="Nilai Rata-rata" readonly>
                            </div>

                            <div class="form-group">
                                <label for="">Masukan</label>
                                <textarea name="masukan" id="" cols="30" rows="10" class="form-control">{{$penilaian->masukan}}</textarea>
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
        $('.form-group').on('input','.nilai', function(){
            var totalSum = 0;
            $('.form-group .nilai').each(function(){
                var inputVal = $(this).val();
                if($.isNumeric(inputVal)){
                    totalSum += parseFloat(inputVal);
                }
            });
            $('#result').val(totalSum);
            var rata = totalSum/7;
            var mean = rata.toFixed(2);
            $('#rata').val(mean);
        });
    </script>
@endsection