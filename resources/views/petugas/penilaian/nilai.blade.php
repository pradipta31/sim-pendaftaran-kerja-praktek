@extends('layouts.master',['activeMenu' => 'penilaian'])
@section('title','Data Penilaian')
@section('breadcrumb', 'Data Penilaian')
@section('detail_breadcrumb', 'Data Penilaian Mahasiswa Kerja Praktek')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            
                            <div class="col-xs-2">
                                <b>Nama / NIM</b> <br>
                                <b>Total Nilai</b><br>
                                <b>Nilai Rata-rata</b><br>
                                (NRR = Total Nilai/7)
                            </div>
                            <div class="col-xs-4">
                                : {{$mhs->user->nama}} /{{$mhs->nim}} <br>
                                : 
                                @if ($p == FALSE)
                                    -
                                @else
                                    {{$penilaian->total_nilai}}
                                @endif 
                                <br>
                                : 
                                @if ($p == FALSE)
                                    -
                                @else
                                    {{$penilaian->rata_rata}}
                                @endif 
                                <br>
                            </div>
                            <div class="col-xs-6">
                                @if ($p == FALSE)
                                    <a href="{{url('petugas/penilaian/'.$mhs->id.'/tambah')}}" class="btn btn-primary btn-md pull-right">
                                        <i class="fa fa-tasks"></i>
                                        Tambah Penilaian
                                    </a>
                                @else
                                    <a href="{{url('petugas/penilaian/'.$mhs->id.'/cetak')}}" target="_blank" class="btn btn-primary btn-md pull-right" style="margin-left: 10px">
                                        <i class="fa fa-print"></i>
                                        Cetak Data Penilaian
                                    </a>
                                    <a href="{{url('petugas/penilaian/'.$mhs->id.'/edit')}}" class="btn btn-warning btn-md pull-right">
                                        <i class="fa fa-pencil"></i>
                                        Edit Penilaian
                                    </a>
                                @endif
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Materi Penilaian</th>
                                        <th>Rentang Nilai</th>
                                        <th>Nilai</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td>Kemampuan Beradaptasi</td>
                                        <td>0 - 100</td>
                                        <td>
                                            @if ($p == FALSE)
                                                -
                                            @else
                                                {{$penilaian->adaptasi}}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>Kemampuan Kerjasama Dalam kelompok</td>
                                        <td>0 - 100</td>
                                        <td>
                                            @if ($p == FALSE)
                                                -
                                            @else
                                                {{$penilaian->kerjasama}}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>Kesungguhan Dalam Bekerja</td>
                                        <td>0 - 100</td>
                                        <td>
                                            @if ($p == FALSE)
                                                -
                                            @else
                                                {{$penilaian->kesungguhan}}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4.</td>
                                        <td>Kehadiran</td>
                                        <td>0 - 100</td>
                                        <td>
                                            @if ($p == FALSE)
                                                -
                                            @else
                                                {{$penilaian->kehadiran}}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5.</td>
                                        <td>Tanggung Jawab Terhadap Pekerjaan</td>
                                        <td>0 - 100</td>
                                        <td>
                                            @if ($p == FALSE)
                                                -
                                            @else
                                                {{$penilaian->tanggung_jawab}}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>6.</td>
                                        <td>Pemahaman Tugas</td>
                                        <td>0 - 100</td>
                                        <td>
                                            @if ($p == FALSE)
                                                -
                                            @else
                                                {{$penilaian->pemahaman_tugas}}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>7.</td>
                                        <td>Inisiatif</td>
                                        <td>0 - 100</td>
                                        <td>
                                            @if ($p == FALSE)
                                                -
                                            @else
                                                {{$penilaian->inisiatif}}
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelMahasiswa').dataTable()
        });

        function editUserModal(){
            $('#editUser').modal('show');
        }
    </script>
@endsection
