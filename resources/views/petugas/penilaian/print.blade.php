<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
    <style>
        /* table, td, th {
            border: 1px solid black;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        } */
        .tb{
            border: 1px solid black;
        }
    </style>
	<center>
		<h5 style="text-decoration: underline">FORMULIR PENILAIAN PEMBINA 
            <br>
            KERJA PRAKTEK (KP)</h4>
    </center>
    <br>
    <small>
        <table>
            <tbody>
                <tr>
                    <td>Nama Perusahaan/ Instansi</td><td> : </td><td>KPU Provinsi Bali</td>
                </tr>
                <tr>
                    <td>Alamat Perusahaan/ Instansi</td><td> : </td><td>Jl. Cok Agung Tresna No.8 Dangin Puri Klod, Kec.Denpasar</td>
                </tr>
                <tr>
                    <td> No. Telp/ Faks</td><td> : </td><td>0361 261000</td>
                </tr>
                <tr>
                    <td> Pelaksanaan KP</td><td> : </td><td>{{date('d-m-Y', strtotime($penilaian->mahasiswa->tgl_mulai))}} s/d {{date('d-m-Y', strtotime($penilaian->mahasiswa->tgl_selesai))}}</td>
                </tr>
                <tr>
                    <td>NIM</td><td> : </td><td>{{$penilaian->mahasiswa->nim}}</td>
                </tr>
                <tr>
                    <td>Nama Mahasiswa</td><td> : </td><td>{{$penilaian->mahasiswa->user->nama}}</td>
                </tr>
                <tr>
                    <td>Program Studi</td><td> : </td><td>{{$penilaian->mahasiswa->pendaftaran->program_studi}}</td>
                </tr>
            </tbody>
        </table>
    </small>

    <small style="font-size: 10px">Penilaian Kerja praktek, unsur-unsur yang dinilai oleh perusahaan/ instansi, antara lain:</small>

	<small>
        <table style="width: 100%; border-collapse: collapse; border: 1px solid black; text-align: center">
            <thead>
                <tr>
                    <th style="border: 1px solid black"><center>No</center></th>
                    <th style="border: 1px solid black"><center>Materi Yang Dinilai</center></th>
                    <th style="border: 1px solid black"><center>Nilai</center></th>
                    <th style="border: 1px solid black"><center>Nilai Satuan</center></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="border: 1px solid black">1.</td>
                    <td style="border: 1px solid black">Kemampuan Beradaptasi</td>
                    <td style="border: 1px solid black">0 - 100</td>
                    <td style="border: 1px solid black">
                        {{$penilaian->adaptasi}}
                    </td>
                </tr>
                <tr>
                    <td style="border: 1px solid black">2.</td>
                    <td style="border: 1px solid black">Kemampuan Kerjasama Dalam Kelompok</td>
                    <td style="border: 1px solid black">0 - 100</td>
                    <td style="border: 1px solid black">
                        {{$penilaian->kerjasama}}
                    </td>
                </tr>
                <tr>
                    <td style="border: 1px solid black">3.</td>
                    <td style="border: 1px solid black">Kesungguhan Dalam Bekerja</td>
                    <td style="border: 1px solid black">0 - 100</td>
                    <td style="border: 1px solid black">
                        {{$penilaian->kesungguhan}}
                    </td>
                </tr>
                <tr>
                    <td style="border: 1px solid black">4.</td>
                    <td style="border: 1px solid black">Kehadiran</td>
                    <td style="border: 1px solid black">0 - 100</td>
                    <td style="border: 1px solid black">
                        {{$penilaian->kehadiran}}
                    </td>
                </tr>
                <tr>
                    <td style="border: 1px solid black">5.</td>
                    <td style="border: 1px solid black">Tangggung Jawan Terhadap Pekerjaan</td>
                    <td style="border: 1px solid black">0 - 100</td>
                    <td style="border: 1px solid black">
                        {{$penilaian->tanggung_jawab}}
                    </td>
                </tr>
                <tr>
                    <td style="border: 1px solid black">6.</td>
                    <td style="border: 1px solid black">Pemahaman Tugas</td>
                    <td style="border: 1px solid black">0 - 100</td>
                    <td style="border: 1px solid black">
                        {{$penilaian->pemahaman_tugas}}
                    </td>
                </tr>
                <tr>
                    <td style="border: 1px solid black">7.</td>
                    <td style="border: 1px solid black">Inisiatif</td>
                    <td style="border: 1px solid black">0 - 100</td>
                    <td style="border: 1px solid black">
                        {{$penilaian->inisiatif}}
                    </td>
                </tr>
                <tr>
                    <td rowspan="3" colspan="2"></td>
                </tr>
                <tr>
                    <td style="border: 1px solid black"><b>Total Nilai</b></td>
                    <td style="border: 1px solid black"><b>{{$penilaian->total_nilai}}</b></td>
                </tr>
                <tr>
                    <td style="border: 1px solid black"><b>Nilai Rata-rata</b></td>
                    <td style="border: 1px solid black"><b>{{$penilaian->rata_rata}}</b></td>
                </tr>
            </tbody>
        </table>
    </small>
    <br>
    @php
        $tgl = date('Y-m-d');
    @endphp
    <small>
        <table>
            <tbody>
                <tr>
                    <td>Keterangan Nilai</td>
                    <td></td>
                    <td></td>
                    <td width="200px"></td>
                    <td>Denpasar, {{\Carbon\Carbon::now()->isoFormat('D MMMM Y')}}</td>
                </tr>
                <tr>
                    <td>85-100</td><td> : </td><td>Istimewa</td>
                    <td width="200px"></td>
                    <td>Pembina</td>
                </tr>
                <tr>
                    <td> 80-85</td><td> : </td><td>Baik Sekali</td>
                </tr>
                <tr>
                    <td> 70-80</td><td> : </td><td>Baik</td>
                </tr>
                <tr>
                    <td>65-70</td><td> : </td><td>Cukup Baik</td>
                </tr>
                <tr>
                    <td>55-65</td><td> : </td><td>Cukup</td>
                    <td width="200px"></td>
                    <td style="text-decoration: underline">(Ni Made Reponi, S.E., M.Si)</td>
                </tr>
                <tr>
                    <td>40-55</td><td> : </td><td>Kurang</td>
                </tr>
                <tr>
                    <td>0-40</td><td> : </td><td>Gagal</td>
                </tr>
            </tbody>
        </table>
    </small>

</body>
</html>