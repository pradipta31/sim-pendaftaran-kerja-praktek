<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Auth::routes(['register' => false, 'verify' => true]);

Route::get('/admin', function(){
    return redirect('/home');
});

Route::get('json', 'JsonController@index');
Route::get('registrasi', 'RegisterController@index');
Route::post('registrasi', 'RegisterController@simpanPendaftaran');
Route::get('registrasi/berhasil', 'RegisterController@berhasil');

Route::group(['middleware' => 'auth'], function(){
    Route::get('/home', 'HomeController@index')->name('home');

    // GROUP PETUGAS
    Route::group(['namespace' => 'Petugas', 'prefix' => 'petugas'], function(){

        // ROUTE PROFILE
        Route::get('profile', 'ProfileController@index');
        Route::put('profile/bio/{id}/edit', 'ProfileController@updateProfile');
        Route::put('profile/avatar/{id}/edit', 'ProfileController@updateAvatar');

        // ROUTE PENGGUNA
        Route::get('pengguna/tambah', 'PenggunaController@tambahPengguna');
        Route::post('pengguna/tambah', 'PenggunaController@simpanPengguna');
        Route::get('pengguna', 'PenggunaController@indexPengguna');
        Route::get('pengguna/{id}/edit', 'PenggunaController@editPengguna');
        Route::put('pengguna/{id}/edit', 'PenggunaController@updatePengguna');

        // ROUTE MAHASISWA
        Route::get('mahasiswa', 'MahasiswaController@index');
        Route::get('mahasiswa/{id}/edit', 'MahasiswaController@edit');
        Route::put('mahasiswa/{id}/edit', 'MahasiswaController@update');
        Route::put('mahasiswa/chpass/{id}', 'MahasiswaController@chPass');
        
        // ROUTE PENDAFTARAN
        Route::get('pendaftaran', 'PendaftaranController@index');
        Route::get('pendaftaran/detail/{id}', 'PendaftaranController@detail');
        Route::get('pendaftaran/download/{id}', 'PendaftaranController@download')->name('surat.download');
        Route::put('pendaftaran/setuju/{id}', 'PendaftaranController@setujuPendaftaran');
        Route::put('pendaftaran/tolak/{id}', 'PendaftaranController@tolakPendaftaran');

        // ROUTE PENILAIAN
        Route::get('penilaian', 'PenilaianController@dataMhs');
        Route::get('penilaian/{id}/mahasiswa', 'PenilaianController@nilaiMhs');
        Route::get('penilaian/{id}/tambah', 'PenilaianController@tambahNilai');
        Route::post('penilaian/tambah', 'PenilaianController@simpanNilai');
        Route::get('penilaian/{id}/edit', 'PenilaianController@editNilai');
        Route::put('penilaian/{id}/edit', 'PenilaianController@updateNilai');
        Route::get('penilaian/{id}/cetak', 'PenilaianController@cetakPdf');

        // ROUTE PENGUMUMAN
        Route::get('pengumuman/tambah', 'PengumumanController@tambahPengumuman');
        Route::post('pengumuman/tambah', 'PengumumanController@simpanPengumuman');
        Route::get('pengumuman', 'PengumumanController@indexPengumuman');
        Route::get('pengumuman/{id}/edit', 'PengumumanController@editPengumuman');
        Route::put('pengumuman/{id}/edit', 'PengumumanController@updatePengumuman');

        // ROUTE JURNAL DAN ABSEN
        Route::get('jurnal', 'JurnalController@showMhs');
        Route::get('mahasiswa/{id}/jurnal', 'JurnalController@indexJurnal');
        Route::get('mahasiswa/{id}/absensi', 'JurnalController@indexAbsen');
        Route::put('jurnal/setuju/{id}', 'JurnalController@validasiJurnal');
        Route::put('jurnal/tolak/{id}', 'JurnalController@tolakJurnal');
        Route::get('jurnal/cetak/{id}', 'JurnalController@cetakJurnal');
        Route::get('absen/cetak/{id}', 'JurnalController@cetakAbsen');
    });

    // GROUP KEPALA BAGIAN
    Route::group(['namespace' => 'Kabag', 'prefix' => 'kabag'], function(){
        Route::get('pendaftaran', 'PendaftaranController@index');
        Route::get('pendaftaran/detail/{id}', 'PendaftaranController@detail');
        Route::get('mahasiswa', 'MahasiswaController@index');
        Route::get('jurnal', 'JurnalController@showMhs');
        Route::get('mahasiswa/{id}/jurnal', 'JurnalController@indexJurnal');
        Route::get('mahasiswa/{id}/absensi', 'JurnalController@indexAbsen');
        Route::get('penilaian', 'PenilaianController@dataMhs');
        Route::get('penilaian/{id}/mahasiswa', 'PenilaianController@nilaiMhs');
    });

    // GROUP MAHASISWA
    Route::group(['namespace' => 'Mahasiswa', 'prefix' => 'mahasiswa'], function(){
        // ROUTE JURNAL DAN ABSENSI
        Route::get('jurnal/tambah', 'JurnalController@tambahJurnal');
        Route::post('jurnal/tambah', 'JurnalController@simpanJurnal');
        Route::get('jurnal', 'JurnalController@indexJurnal');
        Route::get('absensi', 'JurnalController@indexAbsen');

        // ROUTE PROFILE
        Route::get('profile-saya', 'ProfileController@index');
        Route::put('avatar/{id}/edit', 'ProfileController@updateAvatar');
        Route::put('profile-saya/{id}/edit', 'ProfileController@updateProfile');
    });
});
